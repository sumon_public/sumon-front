# Sumon-front

Install https://trunkrs.dev and add wasm32 build target
```shell
rustup update
cargo install trunk
rustup target add wasm32-unknown-unknown
```

Optional install nightly toolchain and add nightly leptos feature to use nicer function-call syntax https://leptos-rs.github.io/leptos/02_getting_started.html

## Run on dev using
```shell
trunk serve --open
```
`--open` automatically opens a browser window. Otherwise use plain `trunk serve`.

## Use production proxy app
To use production proxy app set `backend` entry in `[[proxy]]` section of `Trunk.toml` to the production URL.

https://leptos.dev/

https://trunkrs.dev/

https://tailwindcss.com/docs/installation