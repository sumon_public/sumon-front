/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,rs}"],
  theme: {
    extend: {
      screens: {
        'tall': { 'raw': '(min-height: 800px)' }, // => @media (min-height: 800px) { ... }
        'tall-extra': { 'raw': '(min-height: 1000px)' }, // => @media (min-height: 1000px) { ... }
        'narrow': { 'raw': '(max-width: 1199px)' },
        'wide': { 'raw': '(min-width: 1200px)' },
      }
    }
  },
  plugins: [
    'prettier-plugin-tailwindcss'
  ],
  darkMode: 'class',
  safelist: [
    'cursor-not-allowed', 'cursor-progress', 'ml-auto', 'wide:w-full', '!justify-end', 'narrow:!grid-rows-[1fr]',
    '!block', 'rotate-0', 'mt-1', '-mb-1', '!grid-rows-[1fr]',
    'narrow:bg-gray-100', 'narrow:dark:bg-gray-700', 'narrow:dark:text-white', 'wide:dark:text-blue-500',
    'border-yellow-500', 'border-green-500', 'border-red-500',
    'opacity-60'
  ]
}