use leptos::*;

#[component]
pub fn Alert(message: String) -> impl IntoView {
    let message_clone = message.clone();
    view! {
        <div
            class="border-l-4 border-rose-500 bg-red-100 text-rose-700 p-2 dark:bg-rose-950 dark:border-rose-700 dark:text-rose-200"
            role="alert"
            class:hidden=move || message.is_empty()
        >
            <p class="font-bold">{message_clone}</p>
        </div>
    }
}

#[component]
pub fn Warn(message: String) -> impl IntoView {
    let message_clone = message.clone();
    view! {
        <div
            class="border-l-4 border-purple-500 bg-purple-100 text-purple-700 p-2 dark:bg-purple-950 dark:border-purple-700 dark:text-purple-200"
            role="alert"
            class:hidden=move || message.is_empty()
        >
            <p class="font-bold">{message_clone}</p>
        </div>
    }
}
