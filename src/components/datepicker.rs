use chrono::{offset::LocalResult, DateTime, Local, NaiveDateTime, TimeZone};
use chrono::{NaiveDate, NaiveTime};
use ev::CustomEvent;
use gloo_utils::format::JsValueSerdeExt;
use leptos::html::Input;
use leptos::*;
use serde::{Deserialize, Serialize};

use crate::initDatePicker;

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
pub enum PickLevel {
    Day,
    Month,
    Year,
}

#[component]
pub fn DatePicker(
    id: String,
    datetime: RwSignal<DateTime<Local>>,
    pick_level: PickLevel,
) -> impl IntoView {
    // let on_change_closure = move |ev| {
    //     leptos::logging::log!("Datepicker change event! {:?}", ev);
    //     let input_value = event_target_value(&ev);
    //     if let Ok(naive_date) = NaiveDate::parse_from_str(&input_value, "%Y-%m-%d") {
    //         if let Some(naive_time) = NaiveTime::from_hms_opt(0, 0, 0) {
    //             let naive_datetime = NaiveDateTime::new(naive_date, naive_time);
    //             let local_datetime_result = Local.from_local_datetime(&naive_datetime);
    //             match local_datetime_result {
    //                 LocalResult::Single(local_datetime) => datetime.set(local_datetime),
    //                 _ => {}
    //             };
    //         }
    //     }
    // };

    let datepicker_input: NodeRef<Input> = create_node_ref();
    let on_my_change_date_event = move |ev: CustomEvent| {
        ev.stop_immediate_propagation();
        #[derive(Deserialize, Debug)]
        struct ChangeDate {
            value: String,
        }
        let ev_detail: ChangeDate = ev.detail().into_serde().unwrap();
        if let Ok(naive_date) = NaiveDate::parse_from_str(&ev_detail.value, "%Y-%m-%d") {
            if let Some(naive_time) = NaiveTime::from_hms_opt(0, 0, 0) {
                let naive_datetime = NaiveDateTime::new(naive_date, naive_time);
                let local_datetime_result = Local.from_local_datetime(&naive_datetime);
                match local_datetime_result {
                    LocalResult::Single(local_datetime) => datetime.set(local_datetime),
                    _ => {}
                };
            }
        }
    };

    view! {
        <input
            node_ref=datepicker_input
            id=id
            type="text"
            value=move || datetime.get().format("%Y-%m-%d").to_string()
            // on:change=on_change_closure
            on:myChangeDateEvent=on_my_change_date_event
            // class="bg-gray-50 border border-gray-300 text-gray-900 text-md focus:ring-blue-500 focus:border-blue-500 block w-full dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            class="w-[90px] h-6 font-bold bg-transparent focus:outline-none border-none text-center"
        />
    }
    .on_mount(move |el| {
        if let Some(element_id) = el.get_attribute("id") {
            initDatePicker(
                &element_id,
                match &pick_level {
                    PickLevel::Day => 0,
                    PickLevel::Month => 1,
                    PickLevel::Year => 2,
                },
            );
        }
    })
}
