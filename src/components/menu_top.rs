use crate::{api_query, switchDarkMode, GlobalState};
use gloo_storage::*;
use leptos::*;
use leptos_query::*;
use leptos_router::{use_router, A};
use sumon_common::{BranchData, BranchState, UserData};

#[component]
pub fn MenuTop() -> impl IntoView {
    let state = expect_context::<RwSignal<GlobalState>>();
    // `create_slice` lets us create a "lens" into the data
    let (authenticated, _set_authenticated) = create_slice(
        // we take a slice *from* `state`
        state,
        // our getter returns a "slice" of the data
        |state| state.authenticated,
        // our setter describes how to mutate that slice, given a new value
        |state, n| state.authenticated = n,
    );

    // `create_slice` lets us create a "lens" into the data
    let (_global_dark_mode_read, global_dark_mode_write) = create_slice(
        // we take a slice *from* `state`
        state,
        // our getter returns a "slice" of the data
        |state| state.dark_mode,
        // our setter describes how to mutate that slice, given a new value
        |state, n| state.dark_mode = n,
    );

    let (menu_expanded, set_menu_expanded) = create_signal(false);

    let (dark_mode, set_dark_mode) =
        create_signal(match gloo_storage::LocalStorage::get("dark_mode") {
            Ok(Some(dark_mode)) => {
                global_dark_mode_write.set(Some(dark_mode));
                dark_mode
            }
            _ => true,
        });
    switchDarkMode(dark_mode.get_untracked()); // set initial mode
    let on_dark_switch = move |_| {
        leptos::logging::log!("Dark mode: {}", dark_mode.get());
        let new_mode = !dark_mode.get();
        let _ = gloo_storage::LocalStorage::set("dark_mode", new_mode);
        set_dark_mode.set(new_mode);
        global_dark_mode_write.set(Some(new_mode));
        switchDarkMode(new_mode);
        set_menu_expanded.set(false);
    };

    let QueryResult {
        data: user_result,
        // is_loading,
        // is_fetching,
        // is_stale: user_is_stale,
        ..
    } = api_query::use_user_query(move || "authenticated_user".to_string());

    let QueryResult {
        data: user_branch_list_result,
        ..
    } = api_query::use_user_branch_list_query(move || "authenticated_user_branch_list".to_string());

    let (user_branch_list_dropdown_expanded, set_user_branch_list_dropdown_expanded) =
        create_signal(false);

    let menu_toggle = move |_| {
        set_menu_expanded.set(!menu_expanded.get());
    };
    let menu_hide = move |_| {
        set_user_branch_list_dropdown_expanded.set(false);
        set_menu_expanded.set(false);
    };

    let router = use_router();
    let router_pathname = router.pathname();
    let is_branch_route = create_memo(move |_| router_pathname.get().starts_with("/branch/"));

    view! {
        <nav class="bg-white border-gray-200 dark:bg-gray-900">
            <div
                class="max-w-screen-xl flex flex-wrap items-end justify-between mx-auto p-4"
                class=("!justify-end", move || !authenticated.get())
            >
                <Show when=move || authenticated.get() fallback=|| view! { "" }>
                    <a href="/home" class="flex items-center">
                        <img src="/images/logo.svg" class="h-8 mr-3" alt="Sumon logo"/>
                        <span class="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">
                            Sumon
                        </span>
                    </a>
                </Show>

                // <img class="w-8 h-8 rounded-full" src="/docs/images/people/profile-picture-3.jpg" alt="user photo">

                // Dropdown menu

                // www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">

                <button
                    class="wide:hidden narrow:ml-auto"
                    on:click=menu_toggle
                    aria-expanded=move || if menu_expanded.get() { "true" } else { "false" }
                >
                    <span class="sr-only">Open main menu</span>
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        class="h-6 w-6 text-gray-600 dark:text-gray-300"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                    >
                        <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M4 6h16M4 12h16M4 18h16"
                        ></path>
                    </svg>
                </button>
                <div
                    class="narrow:w-full narrow:grid narrow:grid-rows-[0fr] transition-[grid-template-rows] duration-300 ease-out wide:grid-rows-[1fr]"
                    class=("narrow:!grid-rows-[1fr]", move || menu_expanded.get())
                    class:wide:w-full=move || !authenticated.get()
                >
                    <div class="narrow:overflow-hidden">
                        <div
                            class="wide:w-auto items-end justify-end w-full wide:!flex wide:order-1 font-medium p-4 wide:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 wide:flex-row wide:space-x-8 wide:mt-0 wide:border-0 wide:bg-white dark:bg-gray-800 wide:dark:bg-gray-900 dark:border-gray-700"
                            class:wide:w-full=move || !authenticated.get()
                        >
                            <Show when=move || authenticated.get() fallback=|| view! { "" }>
                                <ul class="flex flex-col wide:flex-row wide:space-x-8">
                                    <li>
                                        <A
                                            href="/home"
                                            on:click=menu_hide
                                            class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 wide:hover:bg-transparent wide:hover:text-blue-700 wide:p-0 dark:text-white wide:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white wide:dark:hover:bg-transparent dark:border-gray-700"
                                            active_class="narrow:bg-gray-100 narrow:dark:bg-gray-700 narrow:dark:text-white wide:dark:text-blue-500"
                                        >
                                            Home
                                        </A>
                                    </li>
                                    <Transition fallback=move || {
                                        view! { <li>"Loading..."</li> }
                                    }>
                                        {move || {
                                            user_branch_list_result
                                                .get()
                                                .map(api_query::handle_unauthorized::<Vec<BranchData>>)
                                                .map(|user_branch_list_result| {
                                                    leptos::logging::log!(
                                                        "handling user_branch_list_result: {}",
                                                        user_branch_list_result.is_ok()
                                                    );
                                                    match user_branch_list_result {
                                                        Ok(user_branch_list) => {
                                                            let dropdown_elements = user_branch_list
                                                                .into_iter()
                                                                .map(|user_branch: BranchData| {
                                                                    let state_indicator = match user_branch.state {
                                                                        BranchState::New => {
                                                                            view! {
                                                                                <span
                                                                                    class="inline-block w-3 h-3 mx-2 bg-yellow-500 rounded-full"
                                                                                    title="New"
                                                                                ></span>
                                                                            }
                                                                        }
                                                                        BranchState::Online => {
                                                                            view! {
                                                                                <span
                                                                                    class="inline-block w-3 h-3 mx-2 bg-green-500 rounded-full"
                                                                                    title="Online"
                                                                                ></span>
                                                                            }
                                                                        }
                                                                        BranchState::Offline => {
                                                                            view! {
                                                                                <span
                                                                                    class="inline-block w-3 h-3 mx-2 bg-red-500 rounded-full"
                                                                                    title="Offline"
                                                                                ></span>
                                                                            }
                                                                        }
                                                                    };
                                                                    view! {
                                                                        <li class="inline-flex w-full">
                                                                            <A
                                                                                href=format!("/branch/{}", user_branch.id)
                                                                                on:click=menu_hide
                                                                                class="w-full py-1 px-2 narrow:hover:bg-gray-100 narrow:dark:hover:bg-gray-700 narrow:dark:hover:text-white wide:dark:hover:text-blue-500"
                                                                                active_class="narrow:bg-gray-100 narrow:dark:bg-gray-700 narrow:dark:text-white wide:dark:text-blue-500"
                                                                            >
                                                                                {state_indicator}
                                                                                {user_branch.name}
                                                                            </A>
                                                                        </li>
                                                                    }
                                                                })
                                                                .collect_view();
                                                            view! {
                                                                <li>
                                                                    <div class="relative wide:inline-block text-left">
                                                                        <div>
                                                                            <a
                                                                                href="#"
                                                                                class="inline-flex narrow:justify-between w-full py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 wide:hover:bg-transparent wide:hover:text-blue-700 wide:p-0 dark:text-white wide:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white wide:dark:hover:bg-transparent dark:border-gray-700"
                                                                                class=("narrow:bg-gray-100", move || is_branch_route.get())
                                                                                class=(
                                                                                    "narrow:dark:bg-gray-700",
                                                                                    move || {
                                                                                        user_branch_list_dropdown_expanded.get()
                                                                                            | is_branch_route.get()
                                                                                    },
                                                                                )

                                                                                class=(
                                                                                    "narrow:dark:text-white",
                                                                                    move || is_branch_route.get(),
                                                                                )

                                                                                class=(
                                                                                    "wide:dark:text-blue-500",
                                                                                    move || is_branch_route.get(),
                                                                                )

                                                                                on:click=move |_| {
                                                                                    set_user_branch_list_dropdown_expanded
                                                                                        .set(!user_branch_list_dropdown_expanded.get())
                                                                                }
                                                                            >

                                                                                Branches
                                                                                <svg
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    class="w-5 h-5 ml-2 transition-transform transform"
                                                                                    class=(
                                                                                        "rotate-0",
                                                                                        move || user_branch_list_dropdown_expanded.get(),
                                                                                    )

                                                                                    class=(
                                                                                        "mt-1",
                                                                                        move || user_branch_list_dropdown_expanded.get(),
                                                                                    )

                                                                                    class=(
                                                                                        "-mb-1",
                                                                                        move || user_branch_list_dropdown_expanded.get(),
                                                                                    )

                                                                                    class=(
                                                                                        "rotate-180",
                                                                                        move || !user_branch_list_dropdown_expanded.get(),
                                                                                    )

                                                                                    viewBox="0 0 20 20"
                                                                                    fill="currentColor"
                                                                                    aria-hidden="true"
                                                                                >
                                                                                    <path
                                                                                        fill-rule="evenodd"
                                                                                        d="M10 3.293l-6.293 6.293a1 1 0 001.414 1.414L10 5.414l5.879 5.879a1 1 0 001.414-1.414L10 3.293z"
                                                                                    ></path>
                                                                                </svg>
                                                                            </a>
                                                                        </div>
                                                                        <div
                                                                            class="z-[1] wide:origin-top-right wide:absolute right-0 wide:pt-3 grid grid-rows-[0fr] transition-[grid-template-rows] duration-300 ease-out"
                                                                            class=(
                                                                                "!grid-rows-[1fr]",
                                                                                move || user_branch_list_dropdown_expanded.get(),
                                                                            )
                                                                        >

                                                                            <div class="overflow-hidden wide:w-[300px]">
                                                                                <ul class="py-1 overflow-hidden m-1 px-1 wide:rounded-md wide:shadow-lg bg-white dark:bg-gray-800 ring-1 ring-gray-300 dark:ring-gray-600 ring-opacity-5">
                                                                                    {dropdown_elements}
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            }
                                                        }
                                                        Err(err) => {
                                                            leptos::logging::log!(" - error: {}", err);
                                                            view! { <li>...</li> }
                                                        }
                                                    }
                                                })
                                        }}

                                    </Transition>
                                    // <li>
                                    // <A
                                    // href="/home"
                                    // on:click=menu_hide
                                    // class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 wide:hover:bg-transparent wide:hover:text-blue-700 wide:p-0 dark:text-white wide:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white wide:dark:hover:bg-transparent dark:border-gray-700"
                                    // active_class="narrow:bg-gray-100 narrow:dark:bg-gray-700 narrow:dark:text-white wide:dark:text-blue-500"
                                    // >
                                    // Something
                                    // </A>
                                    // </li>
                                    <li>
                                        <a
                                            href="/logout"
                                            on:click=menu_hide
                                            class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 wide:hover:bg-transparent wide:hover:text-blue-700 wide:p-0 dark:text-white wide:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white wide:dark:hover:bg-transparent dark:border-gray-700"
                                        >
                                            Log Out
                                        </a>
                                    </li>
                                </ul>

                                <Transition fallback=move || {
                                    view! { <h2>"Loading..."</h2> }
                                }>
                                    {move || {
                                        user_result
                                            .get()
                                            .map(api_query::handle_unauthorized::<UserData>)
                                            .map(|user_result| {
                                                leptos::logging::log!(
                                                    "handling user_result: {}", user_result.is_ok()
                                                );
                                                match user_result {
                                                    Ok(user) => {
                                                        view! {
                                                            <h2 class="text-gray-900 dark:text-white">{user.email}</h2>
                                                        }
                                                    }
                                                    Err(err) => {
                                                        leptos::logging::log!(" - error: {}", err);
                                                        view! { <h2 class="text-gray-900 dark:text-white">...</h2> }
                                                    }
                                                }
                                            })
                                    }}

                                </Transition>
                            </Show>

                            <label
                                class="relative flex items-center cursor-pointer m-2 wide:m-0"
                                class:ml-auto=move || !authenticated.get()
                            >
                                <input
                                    type="checkbox"
                                    value=""
                                    id="input-darkMode"
                                    checked=move || dark_mode.get()
                                    class="sr-only peer"
                                    on:change=on_dark_switch
                                />
                                <div class="w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
                                <span class="ml-3 text-sm font-medium text-gray-900 dark:text-white">
                                    Dark mode
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    }
}
