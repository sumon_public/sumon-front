use gloo_net::http::Request;
use serde::{Serialize, Deserialize};
use serde_json::{json, Value};
use sumon_common::{BranchData, BranchUpdateRequest, Device, SeriesRequest, SeriesResponse, UserData};
use uuid::Uuid;
use std::{fmt::Debug, hash::Hash};

use crate::api_query::{UserBranchDevicesQueryKey, UserBranchQueryKey};

const API_PREFIX: &'static str = "/_";

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum RequestResult<T> {
    Ok(T),
    Unauthorized,
    Err(String),
}

pub async fn login_with_credentials(email: String, password: String) -> Result<String, String> {
    let login_data: Value = json!({
        "email": email,
        "password": password
    });
    match Request::post(&format!("{}{}", API_PREFIX, "/login"))
        .json(&login_data).map_err(|e| e.to_string())?
        .send()
        .await {
        Ok(res) => {
            if res.status().eq(&200) {
                Ok(res.text().await.unwrap_or("OK".to_string()))
            } else {
                Err(res.text().await.unwrap_or("Unexpected server response".to_string()))
            }
        },
        Err(err) => Err(err.to_string()),
    }
}

pub async fn login_with_google(token: String) -> Result<String, String> {
    let login_data: Value = json!({
        "token": token,
    });
    match Request::post(&format!("{}{}", API_PREFIX, "/login-google"))
        .json(&login_data).map_err(|e| e.to_string())?
        .send()
        .await {
        Ok(res) => {
            if res.status().eq(&200) {
                Ok(res.text().await.unwrap_or("OK".to_string()))
            } else {
                #[derive(Deserialize)]
                struct ErrorResponse {msg: String}
                Err(match res.json::<ErrorResponse>().await {
                    Ok(error_response) => error_response.msg,
                    Err(_) => "Unexpected server response".to_string(),
                })
            }
        },
        Err(err) => Err(err.to_string()),
    }
}

pub async fn get_user(_id: String) -> RequestResult<UserData> {
    match Request::get(&format!("{}{}", API_PREFIX, "/user"))
        .send()
        .await {
        Ok(res) => {
            if res.status().eq(&200) {
                match res.json::<UserData>().await {
                    Ok(response) => RequestResult::Ok(response),
                    Err(_) => RequestResult::Err("Could not parse server response".to_string()),
                }
            } else if res.status().eq(&401) {
                // let navigate = leptos_router::use_navigate();
                // let _ = navigate("/", Default::default());
                RequestResult::Unauthorized
            } else {
                #[derive(Deserialize)]
                struct ErrorResponse {msg: String}
                RequestResult::Err(match res.json::<ErrorResponse>().await {
                    Ok(error_response) => error_response.msg,
                    Err(_) => "Unexpected server response".to_string(),
                })
            }
        },
        Err(err) => RequestResult::Err(err.to_string()),
    }
}

pub async fn get_user_branch_list(_id: String) -> RequestResult<Vec<BranchData>> {
    match Request::get(&format!("{}{}", API_PREFIX, "/user-branch-list"))
        .send()
        .await {
        Ok(res) => {
            if res.status().eq(&200) {
                match res.json::<Vec<BranchData>>().await {
                    Ok(response) => RequestResult::Ok(response),
                    Err(_) => RequestResult::Err("Could not parse server response".to_string()),
                }
            } else if res.status().eq(&401) {
                // let navigate = leptos_router::use_navigate();
                // let _ = navigate("/", Default::default());
                RequestResult::Unauthorized
            } else {
                #[derive(Deserialize)]
                struct ErrorResponse {msg: String}
                RequestResult::Err(match res.json::<ErrorResponse>().await {
                    Ok(error_response) => error_response.msg,
                    Err(_) => "Unexpected server response".to_string(),
                })
            }
        },
        Err(err) => RequestResult::Err(err.to_string()),
    }
}

pub async fn get_user_branch(key: UserBranchQueryKey) -> RequestResult<BranchData> {
    let id: String = key.0;
    match Request::get(&format!("{}{}{}", API_PREFIX, "/user-branch/", id))
        .send()
        .await {
        Ok(res) => {
            if res.status().eq(&200) {
                match res.json::<BranchData>().await {
                    Ok(response) => RequestResult::Ok(response),
                    Err(_) => RequestResult::Err("Could not parse server response".to_string()),
                }
            } else if res.status().eq(&401) {
                // let navigate = leptos_router::use_navigate();
                // let _ = navigate("/", Default::default());
                RequestResult::Unauthorized
            } else {
                #[derive(Deserialize)]
                struct ErrorResponse {msg: String}
                RequestResult::Err(match res.json::<ErrorResponse>().await {
                    Ok(error_response) => error_response.msg,
                    Err(_) => "Unexpected server response".to_string(),
                })
            }
        },
        Err(err) => RequestResult::Err(err.to_string()),
    }
}

pub async fn update_user_branch(branch_update_data: BranchUpdateRequest) -> RequestResult<BranchData> {
    let update_data: Value = json!(branch_update_data);
    let request = match Request::post(&format!("{}{}", API_PREFIX, "/user-branch-update"))
        .json(&update_data) {
            Ok(request) => request,
            Err(e) => { return RequestResult::Err(e.to_string()) },
        };
    match request.send()
        .await {
        Ok(res) => {
            if res.status().eq(&200) {
                match res.json::<BranchData>().await {
                    Ok(response) => RequestResult::Ok(response),
                    Err(_) => RequestResult::Err("Could not parse server response".to_string()),
                }
            } else if res.status().eq(&401) {
                // let navigate = leptos_router::use_navigate();
                // let _ = navigate("/", Default::default());
                RequestResult::Unauthorized
            } else {
                #[derive(Deserialize)]
                struct ErrorResponse {msg: String}
                RequestResult::Err(match res.json::<ErrorResponse>().await {
                    Ok(error_response) => error_response.msg,
                    Err(_) => "Unexpected server response".to_string(),
                })
            }
        },
        Err(err) => RequestResult::Err(err.to_string()),
    }
}

pub async fn get_user_branch_device_list(key: UserBranchDevicesQueryKey) -> RequestResult<Vec<Device>> {
    let branch_id: String = key.0;
    match Request::post(&format!("{}{}{}{}", API_PREFIX, "/proxy/", branch_id, "/device-list"))
        .send()
        .await {
        Ok(res) => {
            if res.status().eq(&200) {
                match res.json::<Vec<Device>>().await {
                    Ok(response) => RequestResult::Ok(response),
                    Err(_) => RequestResult::Err("Could not parse server response".to_string()),
                }
            } else if res.status().eq(&401) {
                // let navigate = leptos_router::use_navigate();
                // let _ = navigate("/", Default::default());
                RequestResult::Unauthorized
            } else {
                #[derive(Deserialize)]
                struct ErrorResponse {msg: String}
                RequestResult::Err(match res.json::<ErrorResponse>().await {
                    Ok(error_response) => error_response.msg,
                    Err(_) => "Unexpected server response".to_string(),
                })
            }
        },
        Err(err) => RequestResult::Err(err.to_string()),
    }
}

pub struct LocalDeviceSetParams {
    pub branch_id: String,
    pub device: Device,
}

pub async fn set_local_device(params: LocalDeviceSetParams) -> RequestResult<Device> {
    let update_data: Value = json!(params.device);
    let request = match Request::post(&format!("{}{}{}{}", API_PREFIX, "/proxy/", params.branch_id, "/device-set"))
        .json(&update_data) {
            Ok(request) => request,
            Err(e) => { return RequestResult::Err(e.to_string()) },
        };
    match request.send()
        .await {
        Ok(res) => {
            if res.status().eq(&200) {
                match res.json::<Device>().await {
                    Ok(response) => RequestResult::Ok(response),
                    Err(_) => RequestResult::Err("Could not parse server response".to_string()),
                }
            } else if res.status().eq(&401) {
                // let navigate = leptos_router::use_navigate();
                // let _ = navigate("/", Default::default());
                RequestResult::Unauthorized
            } else {
                #[derive(Deserialize)]
                struct ErrorResponse {msg: String}
                RequestResult::Err(match res.json::<ErrorResponse>().await {
                    Ok(error_response) => error_response.msg,
                    Err(_) => "Unexpected server response".to_string(),
                })
            }
        },
        Err(err) => RequestResult::Err(err.to_string()),
    }
}

pub struct LocalDeviceRemoveParams {
    pub branch_id: String,
    pub device_id: Uuid,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct LocalDeviceRemoveRequestData {
    id: Uuid,
}

pub async fn remove_local_device(params: LocalDeviceRemoveParams) -> RequestResult<Device> {
    let update_data = LocalDeviceRemoveRequestData{id: params.device_id};
    let request = match Request::post(&format!("{}{}{}{}", API_PREFIX, "/proxy/", params.branch_id, "/device-remove"))
        .json(&update_data) {
            Ok(request) => request,
            Err(e) => { return RequestResult::Err(e.to_string()) },
        };
    match request.send()
        .await {
        Ok(res) => {
            if res.status().eq(&200) {
                match res.json::<Device>().await {
                    Ok(response) => RequestResult::Ok(response),
                    Err(_) => RequestResult::Err("Could not parse server response".to_string()),
                }
            } else if res.status().eq(&401) {
                // let navigate = leptos_router::use_navigate();
                // let _ = navigate("/", Default::default());
                RequestResult::Unauthorized
            } else {
                #[derive(Deserialize)]
                struct ErrorResponse {msg: String}
                RequestResult::Err(match res.json::<ErrorResponse>().await {
                    Ok(error_response) => error_response.msg,
                    Err(_) => "Unexpected server response".to_string(),
                })
            }
        },
        Err(err) => RequestResult::Err(err.to_string()),
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct SeriesQueryParams {
    pub branch_id: String,
    pub series_requests: Vec<SeriesRequest>,
}


pub async fn get_series<T>(params: SeriesQueryParams) -> RequestResult<Vec<SeriesResponse<T>>>
where
    T: for<'de> Deserialize<'de> + Debug + Clone + PartialEq + Serialize,
{
    let branch_id = params.branch_id;
    let series_requests = params.series_requests;
    let url = format!("{}{}{}{}", API_PREFIX, "/proxy/", branch_id, "/series");

    let request = match Request::post(&url).json(&series_requests) {
        Ok(req) => req,
        Err(e) => return RequestResult::Err(e.to_string()),
    };

    let response = match request.send().await {
        Ok(res) => res,
        Err(err) => return RequestResult::Err(err.to_string()),
    };

    if response.status() == 200 {
        match response.json::<Vec<SeriesResponse<T>>>().await {
            Ok(response) => RequestResult::Ok(response),
            Err(e) => RequestResult::Err(format!("Could not parse server response: {}", e)),
        }
    } else if response.status() == 401 {
        // let navigate = leptos_router::use_navigate();
        // let _ = navigate("/", Default::default());
        RequestResult::Unauthorized
    } else {
        #[allow(dead_code)]
        #[derive(Deserialize)]
        struct ErrorResponse { msg: String }

        RequestResult::Err(match response.json::<ErrorResponse>().await {
            Ok(error_response) => error_response.msg,
            Err(e) => format!("Unexpected server response: {}", e),
        })
    }
}