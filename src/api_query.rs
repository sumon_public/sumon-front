use leptos_query::*;
use leptos_router::*;
use serde::{Deserialize, Serialize};
use std::{fmt::Debug, time::Duration};
use sumon_common::{BranchData, Device, SeriesResponse, UserData};
// use gloo_utils::*;
use crate::api::*;

pub fn handle_unauthorized<T>(request_result: RequestResult<T>) -> Result<T, String> {
    match request_result {
        RequestResult::Ok(response) => Ok(response),
        RequestResult::Unauthorized => {
            leptos::logging::log!("Redirecting to /logout because of unauthorized api request");
            let navigate = use_navigate(); // prepare navigate so we can redirect from the async block
            let _ = navigate("/logout", Default::default());
            // use js redirect to avoid cache problems (use_query_client().invalidate_all_queries() doesn't seem to help)
            // let _ = window().location().replace("/logout");
            Err("Unauthorized".to_string())
        }
        RequestResult::Err(msg) => Err(msg),
    }
}

pub fn use_user_query(
    id: impl Fn() -> String + 'static,
) -> QueryResult<RequestResult<UserData>, impl RefetchFn> {
    leptos_query::use_query(
        id,
        get_user,
        QueryOptions {
            default_value: None,
            refetch_interval: Some(Duration::from_secs(600)), // None
            resource_option: Some(ResourceOption::NonBlocking),
            stale_time: Some(Duration::from_secs(600)),
            gc_time: Some(Duration::from_secs(600)),
        },
    )
}

pub fn use_user_branch_list_query(
    id: impl Fn() -> String + 'static,
) -> QueryResult<RequestResult<Vec<BranchData>>, impl RefetchFn> {
    leptos_query::use_query(
        id,
        get_user_branch_list,
        QueryOptions {
            default_value: None,
            refetch_interval: Some(Duration::from_secs(600)), // None
            resource_option: Some(ResourceOption::NonBlocking),
            stale_time: Some(Duration::from_secs(600)),
            gc_time: Some(Duration::from_secs(600)),
        },
    )
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct UserBranchQueryKey(pub String);

pub fn use_user_branch_query(
    // id: impl Fn() -> String + 'static,
    key: impl Fn() -> UserBranchQueryKey + 'static,
) -> QueryResult<RequestResult<BranchData>, impl RefetchFn> {
    leptos_query::use_query(
        key,
        get_user_branch,
        QueryOptions {
            default_value: None,
            refetch_interval: Some(Duration::from_secs(600)), // None
            resource_option: Some(ResourceOption::NonBlocking),
            stale_time: Some(Duration::from_secs(600)),
            gc_time: Some(Duration::from_secs(600)),
        },
    )
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct UserBranchDevicesQueryKey(pub String);

pub fn use_user_branch_device_list_query(
    // id: impl Fn() -> String + 'static,
    key: impl Fn() -> UserBranchDevicesQueryKey + 'static,
) -> QueryResult<RequestResult<Vec<Device>>, impl RefetchFn> {
    leptos_query::use_query(
        key,
        get_user_branch_device_list,
        QueryOptions {
            default_value: None,
            refetch_interval: Some(Duration::from_secs(300)), // None
            resource_option: Some(ResourceOption::NonBlocking),
            stale_time: Some(Duration::from_secs(300)),
            gc_time: Some(Duration::from_secs(300)),
        },
    )
}

pub fn use_series_query<T>(
    params: impl Fn() -> SeriesQueryParams + 'static,
) -> QueryResult<RequestResult<Vec<SeriesResponse<T>>>, impl RefetchFn>
where
    T: for<'de> Deserialize<'de> + Debug + Clone + PartialEq + Serialize,
{
    leptos_query::use_query(
        params,
        get_series::<T>,
        QueryOptions {
            default_value: None,
            refetch_interval: Some(Duration::from_secs(300)),
            resource_option: Some(ResourceOption::NonBlocking),
            stale_time: Some(Duration::from_secs(300)),
            gc_time: Some(Duration::from_secs(300)),
        },
    )
}
