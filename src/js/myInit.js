myInit = (callback) => {
    if (typeof rsModule === 'undefined') {
        const jsModuleEl = document.querySelector('html head link[href*="sumon-front"][href$=".js"]');
        console.log(jsModuleEl.getAttribute('href'));
        // set module ref to global variable to be globally accessible as rsModule
        import(jsModuleEl.getAttribute('href')).then(tmpModule => {
            console.log('rsModule loaded!');
            rsModule = tmpModule;
            if (typeof callback !== 'undefined') {
                callback();
            }
        });
    } else {
        callback();
    }
};

googleLoginCallback = (data) => {
    console.log(data);
    // Call the Rust callback function from JavaScript
    // import('/sumon-front.js').then(module => {
    //   module.handle_google_login(data.client_id, data.credential);
    // });
    // const jsModuleEl = document.querySelector('html head link[href*="sumon-front"][href$=".js"]');
    // console.log(jsModuleEl.getAttribute('href'));
    // import(jsModuleEl.getAttribute('href')).then(module => {
    //   module.handle_google_login(data.client_id, data.credential);
    // });
    // Call the Rust callback function from JavaScript
    // rsModule.handle_google_login(data.client_id, data.credential);
    const loginBtn = document.getElementById('googleLoginButton');
    const myGoogleLoginEvent = new CustomEvent("myGoogleLoginEvent", {
        detail: {
            client_id: data.client_id ? data.client_id : null, // when using Google FedCM the client_id might not be directly present
            token: data.credential
        },
    });
    loginBtn.dispatchEvent(myGoogleLoginEvent);
};

ensureScriptIsLoaded = (scriptUrl, callback) => {
    // Check if the script is already present
    if (document.querySelector("script[src='" + scriptUrl + "']")) {
        if (callback && typeof callback === "function") {
            // If script is already loaded, execute the callback immediately
            callback();
        }
        return;
    }
    var script = document.createElement("script");
    script.src = scriptUrl;
    script.async = true;
    script.defer = true;
    script.onload = function () {
        if (callback && typeof callback === "function") {
            // Execute the callback when the script is loaded
            callback();
        }
    };
    // Append the script to the document's head
    var head = document.head || document.getElementsByTagName("head")[0];
    head.appendChild(script);
}

googleInitButton = () => {
    const innerFn = () => {
        myInit(() => {
            const loginBtn = document.getElementById('googleLoginButton');
            google.accounts.id.initialize({
                client_id: loginBtn.dataset.myGoogleClientId,
                context: 'signin',
                ux_mode: 'popup',
                itp_support: true,
                use_fedcm_for_prompt: true, // https://developers.google.com/identity/gsi/web/guides/fedcm-migration
                callback: googleLoginCallback
            });
            google.accounts.id.prompt(); // display the One Tap dialog
            // render standard login button
            google.accounts.id.renderButton(
                loginBtn,
                {
                    type: "standard",
                    shape: "rectangular",
                    theme: "filled_" + (document.getElementsByTagName('html')[0].classList.contains('dark') ? "black" : "blue"),
                    text: "signin_with",
                    size: "medium",
                    logo_alignment: "left",
                    width: Math.min(Math.max(40, loginBtn.offsetWidth), 400)
                }  // customization attributes
            );
        });
    };
    ensureScriptIsLoaded("https://accounts.google.com/gsi/client", innerFn); // load google gsi script only when it is needed
    // othwrwise if index.html already contains <script src="https://accounts.google.com/gsi/client" async defer></script> use:
    // if (document.readyState === "complete") {
    //     innerFn();
    // } else {
    //     window.onload = function () {
    //         innerFn();
    //     };
    // };
};

switchDarkMode = (enable) => {
    const html = document.getElementsByTagName('html')[0];
    if (enable) {
        html.classList.add('dark');
    } else {
        html.classList.remove('dark');
    }
};

function debounce(func, wait) {
    let timeout;
    return function(...args) {
        clearTimeout(timeout);
        timeout = setTimeout(() => func.apply(this, args), wait);
    };
}

initJSChart = (elementID, options, dark_mode) => {
    // Debounced inner function
    const innerFn = debounce(() => {
        console.log("initJSChart elementID:", elementID, "dark_mode:", dark_mode);
        var chartEl = document.getElementById(elementID);
        if (!chartEl) {
            console.error("Element not found:", elementID);
            return;
        }

        let existingInstance = echarts.getInstanceByDom(chartEl);
        if (existingInstance) {
            existingInstance.dispose();
        }
        var myChart = echarts.init(chartEl, dark_mode ? 'dark' : null);

        // remove unit info from legend entries
        options.legend.formatter = function (name) {
            var nameMatch = name.match(/^(.+)(?: \[.*\])$/u);
            return nameMatch[1];
        };

        options && myChart.setOption(options);

        // Custom legend click event handler
        if (options.legend.data.includes('Battery [%]')) {
            myChart.on('legendselectchanged', function (params) {
                var isSelected = params.selected['Battery [%]'];
                myChart.dispatchAction({
                    type: isSelected ? 'legendSelect' : 'legendUnSelect',
                    name: 'Battery min [%]'
                });
                myChart.dispatchAction({
                    type: isSelected ? 'legendSelect' : 'legendUnSelect',
                    name: 'Battery max [%]'
                });
            });
        }

        if (!chartEl.dataset.hasResizeListener) {
            window.addEventListener('resize', (e) => {
                myChart.resize();
            });
            chartEl.dataset.hasResizeListener = true;
        }
    }, 150); // 300ms debounce time, adjust as needed

    // ensureScriptIsLoaded("https://cdn.jsdelivr.net/npm/echarts@5.5.1/dist/echarts.min.js", innerFn); // load echarts script only when it is needed
    // otherwise if index.html already contains <script src="..." async defer></script> use:
    if (document.readyState === "complete") {
        innerFn();
    } else {
        window.onload = function () {
            innerFn();
        };
    };
};

formatDate = (date) => {
    // Format the date as 'YYYY-MM-DD'
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-indexed
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
};

initDatePicker = (elementID, pickLevel) => {
    // wait for datepicker script to load
    const innerFn = () => {
        console.log("Initializing datepicker: ", elementID);
        const element = document.getElementById(elementID);
        new Datepicker(element, {
            "format": "yyyy-mm-dd",
            "autohide": true,
            "todayButton": true,
            "weekStart": 1,
            "todayButtonMode": 1,
            "maxDate": new Date(),
            "pickLevel": pickLevel
        });
        element.addEventListener("changeDate", (e) => {
            let new_value = element.value.trim();
            if (pickLevel === 1) {
                // if pick month is active and user selected current month, than use today instead of first of month
                const today = new Date();
                today.setDate(1); // Set the date to the 1st of the current month
                const first_of_cur_month = formatDate(today);
                if (first_of_cur_month === new_value) {
                    console.log("adjusting month date to: ", formatDate(new Date()));
                    new_value = formatDate(new Date()); // Format as YYYY-MM-DD
                }
            }
            const myChangeDateEvent = new CustomEvent("myChangeDateEvent", {
                detail: {
                    value: new_value,
                }
            });
            element.dispatchEvent(myChangeDateEvent);
        });
    };
    if (document.readyState === "complete") {
        innerFn();
    } else {
        window.onload = function () {
            innerFn();
        };
    };
};