use chrono::{DateTime, Datelike, Days, Local, Months, NaiveDateTime, NaiveTime, TimeZone};
use leptos::*;
use leptos_chartistry::*;
use leptos_meta::Style;
use leptos_query::QueryResult;
use serde::{Deserialize, Deserializer, Serialize};
use std::str::FromStr;
use sumon_common::{
    SeriesRequest, SeriesRequestField, SeriesResponse, TimeSeriesFieldAggregation,
    TimeSeriesFieldUnit, TimeSeriesSampleFill, TimeSeriesSampleUnit,
};
use uuid::Uuid;

use crate::{api::SeriesQueryParams, api_query};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Data {
    pub x: DateTime<Local>,
    #[serde(default)]
    pub consumption: f64,
    #[serde(default)]
    pub solar: f64,
    #[serde(default)]
    #[serde(deserialize_with = "deserialize_battery_soc")]
    pub battery_soc: i32,
}

fn deserialize_battery_soc<'de, D>(deserializer: D) -> Result<i32, D::Error>
where
    D: Deserializer<'de>,
{
    let f = f64::deserialize(deserializer)?;
    Ok(f as i32)
}

#[component]
pub fn EnergyOverview(
    branch_id: Uuid,
    initial_datetime: DateTime<Local>,
    sample_unit: TimeSeriesSampleUnit,
    wide: bool,
) -> impl IntoView {
    const RED: Colour = Colour::from_rgb(230, 94, 110); // #e65e6e
    const YELLOW: Colour = Colour::from_rgb(252, 186, 3); // #fcba03
    const GRAY: Colour = Colour::from_rgb(100, 100, 100);

    let time_zero = NaiveTime::from_hms_opt(0, 0, 0).expect("Failed to create time_zero");
    let datetime_zero_time = initial_datetime.with_time(time_zero).unwrap();

    let (start_time, end_time) = match sample_unit.clone() {
        TimeSeriesSampleUnit::Day => {
            let datetime_zero_day = datetime_zero_time.with_day(1).unwrap();
            (datetime_zero_day, datetime_zero_day + Months::new(1))
        }
        TimeSeriesSampleUnit::Hour => (datetime_zero_time, datetime_zero_time + Days::new(1)),
        _ => {
            unimplemented!("Requested sample_unit not implemented in energy_overview.rs")
        }
    };

    let sample_unit_for_params = sample_unit.clone();
    let series_params = move || SeriesQueryParams {
        branch_id: branch_id.to_string(),
        series_requests: vec![SeriesRequest {
            name: "energy_overview".to_string(),
            start_time: start_time.into(),
            end_time: end_time.into(),
            sample_rate: 1,
            sample_unit: sample_unit_for_params.clone(),
            fields: vec![
                SeriesRequestField {
                    device_id: Uuid::from_str("34a08287-eb0f-4ca8-b9f5-8336a0786495")
                        .expect("Failed to parse Uuid"),
                    field: "AcConsumptionPower".to_string(),
                    as_name: "consumption".to_string(),
                    aggregation: TimeSeriesFieldAggregation::AVG,
                    unit: TimeSeriesFieldUnit::WattHour,
                    fill: TimeSeriesSampleFill::ZERO,
                    device_label_as: None,
                },
                SeriesRequestField {
                    device_id: Uuid::from_str("34a08287-eb0f-4ca8-b9f5-8336a0786495")
                        .expect("Failed to parse Uuid"),
                    field: "PvPower".to_string(),
                    as_name: "solar".to_string(),
                    aggregation: TimeSeriesFieldAggregation::AVG,
                    unit: TimeSeriesFieldUnit::WattHour,
                    fill: TimeSeriesSampleFill::ZERO,
                    device_label_as: Some("label".to_string()),
                },
                SeriesRequestField {
                    device_id: Uuid::from_str("34a08287-eb0f-4ca8-b9f5-8336a0786495")
                        .expect("Failed to parse Uuid"),
                    field: "BatterySoc".to_string(),
                    as_name: "battery_soc".to_string(),
                    aggregation: TimeSeriesFieldAggregation::AVG,
                    unit: TimeSeriesFieldUnit::Percentage,
                    fill: TimeSeriesSampleFill::PREV,
                    device_label_as: None,
                },
            ],
        }],
    };
    let QueryResult {
        data: energy_overview_series_result,
        ..
    } = api_query::use_series_query::<Data>(move || series_params().clone());

    let (debug_signal, _) = create_signal(false);
    let data_signal = move || {
        with!(|energy_overview_series_result| {
            if let Some(result) = energy_overview_series_result.to_owned() {
                match api_query::handle_unauthorized::<Vec<SeriesResponse<Data>>>(result) {
                    Ok(energy_overview_series) => {
                        leptos::logging::log!("energy_overview new data OK");
                        // There was only one series requested so just grab the first SeriesResponse
                        if let Some(first_series) = energy_overview_series.first() {
                            first_series.data.clone()
                        } else {
                            leptos::logging::log!(
                                "Error loading energy overview. Couldn't find first series."
                            );
                            get_dummy_day_data()
                        }
                    }
                    Err(e) => {
                        leptos::logging::log!("Error loading energy overview. Err result: {}", e);
                        get_dummy_day_data()
                    }
                }
            } else {
                leptos::logging::log!("Error loading energy overview. None result.");
                get_dummy_day_data()
            }
        })
    };

    let x_ticks = TickLabels::from_generator(Timestamps::from_period(match sample_unit.clone() {
        TimeSeriesSampleUnit::Hour => Period::Hour,
        TimeSeriesSampleUnit::Day => Period::Day,
        _ => unimplemented!("Missing x_ticks sample_unit implementation."),
    }));

    let battery_series = Series::new(|data: &Data| data.x).line(
        Line::new(|data: &Data| data.battery_soc.into())
            .with_name("Battery SOC [%]")
            .with_marker(MarkerShape::Circle),
    );

    let series = Series::new(|data: &Data| data.x)
        .bar(
            Bar::new(|data: &Data| data.consumption / 1000.0)
                .with_name("Consumption [kW/h]")
                .with_colour(RED),
        )
        .bar(
            Bar::new(|data: &Data| data.solar / 1000.0)
                .with_name("Solar [kW/h]")
                .with_colour(YELLOW),
        );

    view! {
        <Style>
            "
            .dark .widget-chart {
                /*background-color: #2b303b;*/
            
                /* Use 'fill' for filling text colour */
                fill: #c0c5ce;
            
                /* Some elements (e.g., legend and tooltips) use HTML so we
                still still need to set 'color' */
                color: #c0c5ce;
            }
            
            /** We can set stroke (and fill) directly too */
            .dark .widget-chart ._chartistry_grid_line_x {
                stroke: #505050;
            }
            
            /** The tooltip uses inline CSS styles and so must be overridden */
            .dark .widget-chart ._chartistry_tooltip {
                border: 1px solid #c0c5ce !important;
                background-color: #2b303b !important;
            }
            
            
            /** Adjust legend and tooltip taster position */
            .widget-energy-overview ._chartistry_legend ._chartistry_taster,
            .widget-energy-overview ._chartistry_tooltip ._chartistry_taster {
                margin-bottom: -1px;
            }
            
            /** hide default taster from legend and tooltip */
            .widget-energy-overview > ._chartistry:nth-child(2) ._chartistry_legend table > tbody > tr > td ._chartistry_snippet ._chartistry_taster,
            .widget-energy-overview > ._chartistry:nth-child(2) ._chartistry_tooltip table > tbody > tr ._chartistry_snippet ._chartistry_taster {
                display: none;
            }
            /** add padding so the border is not overlapped with text description */
            .widget-energy-overview > ._chartistry:nth-child(2) ._chartistry_legend table > tbody > tr > td ._chartistry_snippet,
            .widget-energy-overview > ._chartistry:nth-child(2) ._chartistry_tooltip table > tbody > tr ._chartistry_snippet {
                padding-left: 8px;
            }
            /** set colored borders for tooltip and legend */
            .widget-energy-overview > ._chartistry:nth-child(2) ._chartistry_legend table > tbody > tr > td:first-child ._chartistry_snippet,
            .widget-energy-overview > ._chartistry:nth-child(2) ._chartistry_tooltip table > tbody > tr:first-child ._chartistry_snippet {
                border-left: 10px solid #e65e6e;
            }
            .widget-energy-overview > ._chartistry:nth-child(2) ._chartistry_legend table > tbody > tr > td:nth-child(2) ._chartistry_snippet,
            .widget-energy-overview > ._chartistry:nth-child(2) ._chartistry_tooltip table > tbody > tr:nth-child(2) ._chartistry_snippet {
                border-left: 10px solid #fcba03;
            }
            /** position legend's second entry further away from the first entry */
            .widget-energy-overview > ._chartistry:nth-child(2) ._chartistry_legend table > tbody > tr > td:nth-child(2) ._chartistry_snippet {
                margin-left: 12px;
            }
            "
        </Style>
        <strong class="block text-center -mt-3 mb-3">
            {start_time.format("%Y-%m-%d").to_string()} " - "
            {end_time.format("%Y-%m-%d").to_string()}
        </strong>
        <div class="widget-chart widget-energy-overview">
            <Chart
                aspect_ratio=AspectRatio::from_env_width_apply_ratio(if wide { 7.0 } else { 6.0 })
                debug=debug_signal
                series=battery_series
                data=data_signal

                // top=RotatedLabel::middle("Battery SOC")
                // top=Legend::middle()
                bottom=Legend::end()
                left=TickLabels::aligned_floats()
                    .with_format(|tick, _formatter| { format!("{:3.0}", tick) })
                // set 3 character width

                inner=[
                    AxisMarker::left_edge().into_inner(),
                    AxisMarker::bottom_edge().into_inner(),
                    YGridLine::default().with_colour(GRAY).into_inner(),
                ]

                tooltip=Tooltip::left_cursor().show_x_ticks(true)
            />
            <Chart
                aspect_ratio=AspectRatio::from_env_width_apply_ratio(if wide { 4.0 } else { 3.0 })
                debug=debug_signal
                series=series
                data=data_signal

                // top=RotatedLabel::middle("Energy consumption and production")
                left=TickLabels::aligned_floats()
                    .with_format(|tick, _formatter| { format!("{:3.0}", tick) })

                bottom=vec![x_ticks.clone().into_edge(), Legend::end().into_edge()]

                inner=[
                    AxisMarker::left_edge().into_inner(),
                    AxisMarker::bottom_edge().into_inner(),
                    YGridLine::default().with_colour(GRAY).into_inner(),
                ]

                tooltip=Tooltip::left_cursor().show_x_ticks(true)
            />
        </div>
    }
}

// fn parse_time(raw: &str) -> DateTime<Local> {
//     Local
//         .from_local_datetime(&NaiveDateTime::parse_from_str(raw, "%Y-%m-%d %H:%M").unwrap())
//         .unwrap()
// }

pub fn get_dummy_day_data() -> Vec<Data> {
    let parse_time = |raw: &str| {
        Local
            .from_local_datetime(&NaiveDateTime::parse_from_str(raw, "%Y-%m-%d %H:%M").unwrap())
            .unwrap()
    };
    vec![
        Data {
            x: parse_time("2023-06-18 00:00"),
            consumption: 0.25 * 1000.0,
            solar: 0.0,
            battery_soc: 80,
        },
        Data {
            x: parse_time("2023-06-18 01:00"),
            consumption: 0.25 * 1000.0,
            solar: 0.0,
            battery_soc: 77,
        },
        Data {
            x: parse_time("2023-06-18 02:00"),
            consumption: 0.25 * 1000.0,
            solar: 0.0,
            battery_soc: 73,
        },
        Data {
            x: parse_time("2023-06-18 03:00"),
            consumption: 0.25 * 1000.0,
            solar: 0.0,
            battery_soc: 70,
        },
        Data {
            x: parse_time("2023-06-18 04:00"),
            consumption: 0.25 * 1000.0,
            solar: 0.0,
            battery_soc: 67,
        },
        Data {
            x: parse_time("2023-06-18 05:00"),
            consumption: 0.25 * 1000.0,
            solar: 0.3 * 1000.0,
            battery_soc: 63,
        },
        Data {
            x: parse_time("2023-06-18 06:00"),
            consumption: 0.5 * 1000.0,
            solar: 0.85 * 1000.0,
            battery_soc: 60,
        },
        Data {
            x: parse_time("2023-06-18 07:00"),
            consumption: 0.35 * 1000.0,
            solar: 2.2 * 1000.0,
            battery_soc: 58,
        },
        Data {
            x: parse_time("2023-06-18 08:00"),
            consumption: 0.85 * 1000.0,
            solar: 2.8 * 1000.0,
            battery_soc: 63,
        },
        Data {
            x: parse_time("2023-06-18 09:00"),
            consumption: 0.55 * 1000.0,
            solar: 3.25 * 1000.0,
            battery_soc: 92,
        },
        Data {
            x: parse_time("2023-06-18 10:00"),
            consumption: 0.67 * 1000.0,
            solar: 3.9 * 1000.0,
            battery_soc: 100,
        },
        Data {
            x: parse_time("2023-06-18 11:00"),
            consumption: 0.9 * 1000.0,
            solar: 7.0 * 1000.0,
            battery_soc: 100,
        },
        Data {
            x: parse_time("2023-06-18 12:00"),
            consumption: 1.7 * 1000.0,
            solar: 6.9 * 1000.0,
            battery_soc: 100,
        },
        Data {
            x: parse_time("2023-06-18 13:00"),
            consumption: 0.85 * 1000.0,
            solar: 5.45 * 1000.0,
            battery_soc: 98,
        },
        Data {
            x: parse_time("2023-06-18 14:00"),
            consumption: 1.1 * 1000.0,
            solar: 4.15 * 1000.0,
            battery_soc: 100,
        },
        Data {
            x: parse_time("2023-06-18 15:00"),
            consumption: 0.4 * 1000.0,
            solar: 4.8 * 1000.0,
            battery_soc: 100,
        },
        Data {
            x: parse_time("2023-06-18 16:00"),
            consumption: 0.22 * 1000.0,
            solar: 2.7 * 1000.0,
            battery_soc: 100,
        },
        Data {
            x: parse_time("2023-06-18 17:00"),
            consumption: 0.95 * 1000.0,
            solar: 2.0 * 1000.0,
            battery_soc: 100,
        },
        Data {
            x: parse_time("2023-06-18 18:00"),
            consumption: 1.3 * 1000.0,
            solar: 1.0 * 1000.0,
            battery_soc: 100,
        },
        Data {
            x: parse_time("2023-06-18 19:00"),
            consumption: 1.5 * 1000.0,
            solar: 0.55 * 1000.0,
            battery_soc: 98,
        },
        Data {
            x: parse_time("2023-06-18 20:00"),
            consumption: 0.3 * 1000.0,
            solar: 0.12 * 1000.0,
            battery_soc: 95,
        },
        Data {
            x: parse_time("2023-06-18 21:00"),
            consumption: 0.3 * 1000.0,
            solar: 0.0,
            battery_soc: 90,
        },
        Data {
            x: parse_time("2023-06-18 22:00"),
            consumption: 0.3 * 1000.0,
            solar: 0.0,
            battery_soc: 84,
        },
        Data {
            x: parse_time("2023-06-18 23:00"),
            consumption: 0.3 * 1000.0,
            solar: 0.0,
            battery_soc: 81,
        },
    ]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_deserializing_incomplete_data() {
        let json_data = r#"
            {
                "x": "2024-06-24T12:34:00+00:00"
            }
        "#;
        let data: Data = serde_json::from_str(json_data).unwrap();
        assert_eq!(
            data,
            Data {
                x: DateTime::parse_from_rfc3339("2024-06-24T12:34:00+00:00")
                    .unwrap()
                    .into(),
                consumption: 0.0,
                solar: 0.0,
                battery_soc: 0
            }
        );

        let json_data = r#"
            {
                "x": "2024-06-24T12:34:00+00:00",
                "consumption": 300.0,
                "battery_soc": 50.5
            }
        "#;
        let data: Data = serde_json::from_str(json_data).unwrap();
        assert_eq!(
            data,
            Data {
                x: DateTime::parse_from_rfc3339("2024-06-24T12:34:00+00:00")
                    .unwrap()
                    .into(),
                consumption: 300.0,
                solar: 0.0,
                battery_soc: 50
            }
        )
    }
}
