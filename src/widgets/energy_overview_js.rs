use chrono::{DateTime, Datelike, Days, FixedOffset, Local, Months, NaiveTime};
use gloo_utils::format::JsValueSerdeExt;
use leptos::*;
use leptos_chartistry::*;
use leptos_query::QueryResult;
use serde::{Deserialize, Deserializer, Serialize};
use serde_json::json;
use sumon_common::{
    SeriesRequest, SeriesRequestField, SeriesResponse, TimeSeriesFieldAggregation,
    TimeSeriesFieldUnit, TimeSeriesSampleFill, TimeSeriesSampleUnit,
};
use uuid::Uuid;
use wasm_bindgen::JsValue;

use crate::{
    api::SeriesQueryParams,
    api_query,
    components::{
        datepicker::{DatePicker, PickLevel},
        loader::SpinningLoader,
    },
    initJSChart, GlobalState,
};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Data {
    pub x: DateTime<Local>,
    #[serde(default)]
    pub consumption: f64,
    #[serde(default)]
    pub solar: f64,
    #[serde(default)]
    #[serde(deserialize_with = "deserialize_battery_soc")]
    pub battery_soc: i32,
    #[serde(default)]
    #[serde(deserialize_with = "deserialize_battery_soc")]
    pub battery_min: i32,
    #[serde(default)]
    #[serde(deserialize_with = "deserialize_battery_soc")]
    pub battery_max: i32,
    #[serde(default)]
    pub plug1: f64,
    #[serde(default)]
    pub plug2: f64,
    #[serde(default)]
    pub plug1_label: String,
    #[serde(default)]
    pub plug2_label: String,
}

fn deserialize_battery_soc<'de, D>(deserializer: D) -> Result<i32, D::Error>
where
    D: Deserializer<'de>,
{
    let f = f64::deserialize(deserializer)?;
    Ok(f as i32)
}

#[derive(Debug, Clone, PartialEq)]
pub struct EnergyOverviewField {
    pub device_id: Uuid,
    pub name: String,
    pub enabled: bool,
}

fn create_series_request(
    fields: Vec<EnergyOverviewField>,
    start_time: DateTime<FixedOffset>,
    end_time: DateTime<FixedOffset>,
    sample_unit: TimeSeriesSampleUnit,
) -> SeriesRequest {
    let mut request_fields: Vec<SeriesRequestField> = vec![];
    let mut unused_plug_fields = vec!["plug2", "plug1"];
    for field in fields {
        match field.name.as_str() {
            "AcConsumptionPower" => request_fields.push(SeriesRequestField {
                device_id: field.device_id,
                field: "AcConsumptionPower".to_string(),
                as_name: "consumption".to_string(),
                aggregation: TimeSeriesFieldAggregation::AVG,
                unit: TimeSeriesFieldUnit::WattHour,
                fill: TimeSeriesSampleFill::ZERO,
                device_label_as: None,
            }),
            "PvPower" => request_fields.push(SeriesRequestField {
                device_id: field.device_id,
                field: "PvPower".to_string(),
                as_name: "solar".to_string(),
                aggregation: TimeSeriesFieldAggregation::AVG,
                unit: TimeSeriesFieldUnit::WattHour,
                fill: TimeSeriesSampleFill::ZERO,
                device_label_as: None,
            }),
            "BatterySoc" => {
                request_fields.push(SeriesRequestField {
                    device_id: field.device_id,
                    field: "BatterySoc".to_string(),
                    as_name: "battery_soc".to_string(),
                    aggregation: TimeSeriesFieldAggregation::AVG,
                    unit: TimeSeriesFieldUnit::Percentage,
                    fill: TimeSeriesSampleFill::PREV,
                    device_label_as: None,
                });
                request_fields.push(SeriesRequestField {
                    device_id: field.device_id,
                    field: "BatterySoc".to_string(),
                    as_name: "battery_min".to_string(),
                    aggregation: TimeSeriesFieldAggregation::MIN,
                    unit: TimeSeriesFieldUnit::Percentage,
                    fill: TimeSeriesSampleFill::PREV,
                    device_label_as: None,
                });
                request_fields.push(SeriesRequestField {
                    device_id: field.device_id,
                    field: "BatterySoc".to_string(),
                    as_name: "battery_max".to_string(),
                    aggregation: TimeSeriesFieldAggregation::MAX,
                    unit: TimeSeriesFieldUnit::Percentage,
                    fill: TimeSeriesSampleFill::PREV,
                    device_label_as: None,
                });
            }
            "power" => {
                let unused_plug_field = unused_plug_fields.pop();
                if let Some(plug_field) = unused_plug_field {
                    request_fields.push(SeriesRequestField {
                        device_id: field.device_id,
                        field: "power".to_string(),
                        as_name: plug_field.to_string(),
                        aggregation: TimeSeriesFieldAggregation::AVG,
                        unit: TimeSeriesFieldUnit::WattHour,
                        fill: TimeSeriesSampleFill::ZERO,
                        device_label_as: Some(format!("{}_label", plug_field)),
                    });
                }
            }
            _ => {} // ignore unknown field name
        };
    }
    SeriesRequest {
        name: "energy_overview".to_string(),
        start_time: start_time,
        end_time: end_time,
        sample_rate: 1,
        sample_unit: sample_unit,
        fields: request_fields,
    }
}

fn calc_datetimes(
    initial_datetime: DateTime<Local>,
    sample_unit: &TimeSeriesSampleUnit,
) -> (DateTime<Local>, DateTime<Local>) {
    let time_zero = NaiveTime::from_hms_opt(0, 0, 0).expect("Failed to create time_zero");
    let datetime_zero_time = initial_datetime.with_time(time_zero).unwrap();
    match sample_unit {
        TimeSeriesSampleUnit::Day => {
            let now: DateTime<Local> = Local::now();
            if datetime_zero_time.year().eq(&now.year())
                && datetime_zero_time.month().eq(&now.month())
                && datetime_zero_time.day() <= 14
            {
                (datetime_zero_time - Months::new(1), datetime_zero_time)
            } else {
                let datetime_zero_day = datetime_zero_time.with_day(1).unwrap();
                (datetime_zero_day, datetime_zero_day + Months::new(1))
            }
        }
        TimeSeriesSampleUnit::Hour => (datetime_zero_time, datetime_zero_time + Days::new(1)),
        _ => {
            unimplemented!("Requested sample_unit not implemented in energy_overview.rs")
        }
    }
}

#[component]
pub fn EnergyOverviewJS(
    branch_id: Uuid,
    name: String,
    initial_datetime: DateTime<Local>,
    sample_unit: TimeSeriesSampleUnit,
    fields: Vec<EnergyOverviewField>,
) -> impl IntoView {
    let (name_read, _) = create_signal(name);

    const RED: Colour = Colour::from_rgb(230, 94, 110); // #e65e6e
    const YELLOW: Colour = Colour::from_rgb(252, 186, 3); // #fcba03
    const GRAY: Colour = Colour::from_rgb(100, 100, 100);

    let time_zero = NaiveTime::from_hms_opt(0, 0, 0).expect("Failed to create time_zero");
    let datetime_zero_time = initial_datetime.with_time(time_zero).unwrap();

    let datetime_signal = create_rw_signal(datetime_zero_time);
    let sample_unit_signal = create_rw_signal(sample_unit.clone());

    let mut allowed_fields: Vec<String> = vec![];
    let mut disabled_fields: Vec<String> = vec![];
    for field in fields.clone() {
        let field_name = match field.name.as_str() {
            "AcConsumptionPower" => "consumption".to_string(),
            "PvPower" => "solar".to_string(),
            "BatterySoc" => "battery_soc".to_string(),
            "power" => {
                if allowed_fields.contains(&"plug1".to_string()) {
                    "plug2".to_string()
                } else {
                    "plug1".to_string()
                }
            }
            _ => continue,
        };
        allowed_fields.push(field_name.clone());
        if !field.enabled {
            disabled_fields.push(field_name.clone());
        }
    }
    let (allowed_fields_read, _) = create_signal(allowed_fields);
    let (disabled_fields_read, _) = create_signal(disabled_fields);

    let sample_unit_for_params = sample_unit.clone();
    let series_params = move || {
        let (start_time, end_time) = calc_datetimes(datetime_signal.get(), &sample_unit);
        SeriesQueryParams {
            branch_id: branch_id.to_string(),
            series_requests: vec![create_series_request(
                fields.clone(),
                start_time.into(),
                end_time.into(),
                sample_unit_for_params.clone(),
            )],
        }
    };
    let QueryResult {
        data: energy_overview_series_result,
        ..
    } = api_query::use_series_query::<Data>(move || series_params().clone());

    view! {
        <div>
            <Transition fallback=move || {
                view! {
                    <div>
                        <div class="w-[10%] max-w-[100px] my-10 mt-[115px] mx-auto align-middle text-gray-500 dark:text-gray">
                            <SpinningLoader show=None/>
                        </div>
                    </div>
                }
            }>
                {move || {
                    energy_overview_series_result
                        .get()
                        .map(api_query::handle_unauthorized::<Vec<SeriesResponse<Data>>>)
                        .map(extract_series)
                        .map(|series_result| match series_result {
                            Ok(series) => {
                                view! {
                                    <>
                                        {move || {
                                            let (start_time, end_time) = calc_datetimes(
                                                datetime_signal.get(),
                                                &sample_unit_signal.get(),
                                            );
                                            let pick_level = match sample_unit_signal.get() {
                                                TimeSeriesSampleUnit::Hour => PickLevel::Day,
                                                TimeSeriesSampleUnit::Day => PickLevel::Month,
                                                TimeSeriesSampleUnit::Month => PickLevel::Year,
                                                _ => PickLevel::Day,
                                            };
                                            view! {
                                                <div>
                                                    <div class="flex justify-between">
                                                        <div class="mr-2">
                                                            <label
                                                                class="py-1 px-1 pr-0 block rounded-tl-md bg-white dark:bg-slate-800 hover:bg-gray-100 dark:hover:bg-gray-700 cursor-pointer"
                                                                title="Choose date"
                                                            >
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    class="h-6 w-6 float-left text-gray-600 dark:text-gray-400"
                                                                    fill="none"
                                                                    viewBox="0 0 24 24"
                                                                    stroke="currentColor"
                                                                    stroke-width="2"
                                                                >
                                                                    <path
                                                                        stroke-linecap="round"
                                                                        stroke-linejoin="round"
                                                                        d="M3 9H21M7 3V5M17 3V5M6 13H8M6 17H8M11 13H13M11 17H13M16 13H18M16 17H18M6.2 21H17.8C18.9201 21 19.4802 21 19.908 20.782C20.2843 20.5903 20.5903 20.2843 20.782 19.908C21 19.4802 21 18.9201 21 17.8V8.2C21 7.07989 21 6.51984 20.782 6.09202C20.5903 5.71569 20.2843 5.40973 19.908 5.21799C19.4802 5 18.9201 5 17.8 5H6.2C5.0799 5 4.51984 5 4.09202 5.21799C3.71569 5.40973 3.40973 5.71569 3.21799 6.09202C3 6.51984 3 7.07989 3 8.2V17.8C3 18.9201 3 19.4802 3.21799 19.908C3.40973 20.2843 3.71569 20.5903 4.09202 20.782C4.51984 21 5.07989 21 6.2 21Z"
                                                                    ></path>
                                                                </svg>
                                                                <DatePicker
                                                                    id=format!("datepicker-{}", name_read.get())
                                                                    datetime=datetime_signal
                                                                    pick_level=pick_level.clone()
                                                                />
                                                            </label>
                                                        </div>
                                                        <span class="h-6 block py-1 px-2 text-center">
                                                            {move || {
                                                                if pick_level.eq(&PickLevel::Day) {
                                                                    view! { <>{start_time.format("%Y-%m-%d").to_string()}</> }
                                                                } else {
                                                                    view! {
                                                                        <>
                                                                            {start_time.format("%Y-%m-%d").to_string()} " - "
                                                                            {end_time.format("%Y-%m-%d").to_string()}
                                                                        </>
                                                                    }
                                                                }
                                                            }}

                                                        </span>
                                                    </div>
                                                    <EnergyOverviewInner
                                                        name=name_read.get()
                                                        series=series.to_owned()
                                                        allowed_fields_read=allowed_fields_read
                                                        disabled_fields_read=disabled_fields_read
                                                    />
                                                </div>
                                            }
                                        }}
                                    </>
                                }
                            }
                            Err(e) => {
                                view! {
                                    <>
                                        <div>{e}</div>
                                    </>
                                }
                            }
                        })
                }}

            </Transition>
        </div>
    }
}

#[component]
pub fn EnergyOverviewInner(
    name: String,
    series: SeriesResponse<Data>,
    allowed_fields_read: ReadSignal<Vec<String>>,
    disabled_fields_read: ReadSignal<Vec<String>>,
) -> impl IntoView {
    let (chart_el_id, _) = create_signal(format!("echart-{}", &name));

    let state = expect_context::<RwSignal<GlobalState>>();
    let dark_mode_read = create_read_slice(state, |state| state.dark_mode);

    // emits "Uncaught RuntimeError: unreachable executed" originating from leptos runtime
    // called `Result::unwrap()` on an `Err` value: OwnerDisposed(Owner(NodeId(386v3)))
    // Transition or leptos query result is processed twice for unknown reason.
    // I couldn't find a solution. Since the app seems to be working lets just move on with live.
    // create_effect(move |_| {
    //     let document = web_sys::window().unwrap().document().unwrap();
    //     if let Some(_element) = document.get_element_by_id(&chart_el_id.get()) {
    //         let dark_mode = match dark_mode_read.get() {
    //             Some(dark_mode) => dark_mode,
    //             None => true,
    //         };
    //         initJSChart(
    //             &chart_el_id.get(),
    //             &create_options(
    //                 series.to_owned(),
    //                 allowed_fields_read.get(),
    //                 disabled_fields_read.get(),
    //                 dark_mode,
    //             ),
    //             dark_mode,
    //         );
    //     } else {
    //         leptos::logging::log!("Chart element is not present!");
    //     }
    // });

    move || {
        let dark_mode = match dark_mode_read.get() {
            Some(dark_mode) => dark_mode,
            None => true,
        };
        let options = create_options(
            series.to_owned(),
            allowed_fields_read.get(),
            disabled_fields_read.get(),
            dark_mode.clone(),
        );
        let document = web_sys::window().unwrap().document().unwrap();
        if let Some(_element) = document.get_element_by_id(&chart_el_id.get()) {
            leptos::logging::log!("Reinitializig chart {}", &chart_el_id.get());
            initJSChart(&chart_el_id.get(), &options, dark_mode.clone());
        }
        view! { <div id=chart_el_id style="width:100%; min-height:350px;"></div> }.on_mount(
            move |arg| {
                leptos::logging::log!("Initial on_mount of {:?}", arg.get_attribute("id"));
                if let Some(element_id) = arg.get_attribute("id") {
                    initJSChart(&element_id, &options, dark_mode.clone());
                }
            },
        )
    }
}

fn extract_series(
    series_responses_result: Result<Vec<SeriesResponse<Data>>, String>,
) -> Result<SeriesResponse<Data>, String> {
    leptos::logging::log!("energy_overview_js new data OK");
    // There was only one series requested so just grab the first SeriesResponse
    match series_responses_result {
        Ok(series_responses) => {
            if let Some(first_series_response) = series_responses.first() {
                Ok(first_series_response.clone())
            } else {
                leptos::logging::log!(
                    "Error loading energy overview JS. Couldn't find first series."
                );
                Err("Failed to extract series data".to_string())
            }
        }
        Err(e) => Err(format!("Failed to extract series:{}", e)),
    }
}

fn create_options(
    series: SeriesResponse<Data>,
    allowed_fields: Vec<String>,
    disabled_fields: Vec<String>,
    dark_mode: bool,
) -> JsValue {
    let mut timestamps: Vec<String> = vec![];
    let mut processed_data: Vec<(String, f64, f64, i32, i32, i32, i32, f64, f64)> = vec![];
    let mut plug1_label: String = "".to_string();
    let mut plug2_label: String = "".to_string();
    let now = Local::now();
    for chunk in series.data.iter() {
        timestamps.push(chunk.x.to_rfc3339());
        // ignore future entries in a dataset
        if chunk.x.le(&now) {
            processed_data.push((
                chunk.x.to_rfc3339(),
                (chunk.consumption / 1000.0 * 100f64).floor() / 100.0,
                (chunk.solar / 1000.0 * 100f64).floor() / 100.0,
                chunk.battery_soc,
                chunk.battery_min,
                chunk.battery_max,
                chunk.battery_min - chunk.battery_max,
                (chunk.plug1 * 100f64).floor() / 100.0,
                (chunk.plug2 * 100f64).floor() / 100.0,
            ));
        }
        if plug1_label.is_empty() && !chunk.plug1_label.is_empty() {
            plug1_label = chunk.plug1_label.clone();
        }
        if plug2_label.is_empty() && !chunk.plug2_label.is_empty() {
            plug2_label = chunk.plug2_label.clone();
        }
    }
    let (timestamp_min, timestamp_max) = match series.sample_unit {
        TimeSeriesSampleUnit::Hour => (timestamps.first(), timestamps.last()), // fixed 24 hours
        _ => (None, None), // otherwise let the scale be determined by actual dataset data
    };

    let plug1_label = format!(
        "{} [Wh]",
        if plug1_label.is_empty() {
            "Plug 1".to_string()
        } else {
            plug1_label
        }
    );
    let plug2_label = format!(
        "{} [Wh]",
        if plug2_label.is_empty() {
            "Plug 2".to_string()
        } else {
            plug2_label
        }
    );

    let mut legend_vec: Vec<String> = vec![];
    for name in allowed_fields.iter() {
        match name.as_str() {
            "consumption" => legend_vec.push("Consumption [kWh]".to_string()),
            "solar" => legend_vec.push("Solar [kWh]".to_string()),
            "battery_soc" => legend_vec.push("Battery [%]".to_string()),
            "plug1" => legend_vec.push(plug1_label.clone()),
            "plug2" => legend_vec.push(plug2_label.clone()),
            _ => {}
        }
    }

    let mut legend_disabled = serde_json::Map::new();
    for name in disabled_fields.iter() {
        match name.as_str() {
            "consumption" => {
                legend_disabled.insert("Consumption [kWh]".to_string(), json!(false));
            }
            "solar" => {
                legend_disabled.insert("Solar [kWh]".to_string(), json!(false));
            }
            "battery_soc" => {
                legend_disabled.insert("Battery [%]".to_string(), json!(false));
            }
            "plug1" => {
                legend_disabled.insert(plug1_label.clone(), json!(false));
            }
            "plug2" => {
                legend_disabled.insert(plug2_label.clone(), json!(false));
            }
            _ => {}
        }
    }

    let mut series_vec: Vec<serde_json::value::Value> = vec![];
    for name in allowed_fields.iter() {
        match name.as_str() {
            "consumption" => series_vec.push(json!({
                "name": "Consumption [kWh]",
                "type": "bar",
                "tooltip": {},
                "itemStyle": {
                    "color": "#e65e6e"
                },
                "encode": {
                    "x": "timestamp",
                    "y": "consumption"
                }
            })),
            "solar" => series_vec.push(json!({
                "name": "Solar [kWh]",
                "type": "bar",
                "tooltip": {},
                "itemStyle": {
                    "color": "#fcba03"
                },
                "encode": {
                    "x": "timestamp",
                    "y": "solar"
                }
            })),
            "battery_soc" => {
                series_vec.push(json!({
                    "name": "Battery [%]",
                    "type": "line",
                    "yAxisIndex": 1,
                    "tooltip": {},
                    "itemStyle": {
                        "color": "#12a5ed"
                    },
                    "encode": {
                        "x": "timestamp",
                        "y": "battery_soc"
                    }
                }));
                series_vec.push(json!({
                    "name": "Battery max [%]",
                    "type": "line",
                    "yAxisIndex": 1,
                    "tooltip": {},
                    "itemStyle": {
                        "color": "#12a5ed55"
                    },
                    "encode": {
                        "x": "timestamp",
                        "y": "battery_max"
                    },
                    "stack": "Battery stack"
                }));
                series_vec.push(json!({
                    "name": "Battery [%]",
                    "type": "line",
                    "yAxisIndex": 1,
                    "encode": {
                        "x": "timestamp",
                        "y": "battery_diff"
                    },
                    "stack": "Battery stack", // stack name
                    "stackStrategy": "positive", // strategy
                    "lineStyle": {
                        "opacity": 0 // hide line
                    },
                    "symbol": "none", // hide symbol
                    "areaStyle": {
                        "color": "#12a5ed33"
                    },
                    "tooltip": {
                        "show": false // hide value on tooltip
                    }
                }));
                series_vec.push(json!({
                    "name": "Battery min [%]",
                    "type": "line",
                    "yAxisIndex": 1,
                    "tooltip": {},
                    "itemStyle": {
                        "color": "#12a5ed55"
                    },
                    "encode": {
                        "x": "timestamp",
                        "y": "battery_min"
                    }
                }));
            }
            "plug1" => series_vec.push(json!({
                "name": plug1_label.clone(),
                "type": "line",
                "yAxisIndex": 2,
                "tooltip": {},
                "itemStyle": {
                    "color": "#619c45"
                },
                "encode": {
                    "x": "timestamp",
                    "y": "plug1"
                }
            })),
            "plug2" => series_vec.push(json!({
                "name": plug2_label.clone(),
                "type": "line",
                "yAxisIndex": 2,
                "tooltip": {},
                "itemStyle": {
                    "color": "#91cc75"
                },
                "encode": {
                    "x": "timestamp",
                    "y": "plug2"
                }
            })),
            _ => {}
        }
    }

    JsValue::from_serde(&json!({
        "dataset": {
            "source": processed_data,
            "dimensions": ["timestamp", "consumption", "solar", "battery_soc", "battery_min", "battery_max", "battery_diff", "plug1", "plug2"]
        },
        "grid": {
            "left": 30,
            "top": 55,
            "right": if !allowed_fields.contains(&"battery_soc".to_string()) || (!allowed_fields.contains(&"plug1".to_string()) && !allowed_fields.contains(&"plug2".to_string())) {45} else {85},
            "bottom": 65
        },
        "backgroundColor": "transparent",
        "tooltip": {
            "trigger": "axis",
            "axisPointer": {
                "type": "cross",
                "crossStyle": {
                    "color": "#999"
                }
            },
            "backgroundColor": if dark_mode {"#1f2937"} else {"#ffffff"},
            "borderColor": if dark_mode {"#666"} else {"#e5e7eb"},
            "shadowColor": if dark_mode {"#111"} else {"#aaa"},
            "textStyle": {
                "color": if dark_mode {"#c0c5ce"} else {"#333"}
            }
        },
        "axisPointer": {
            "label": {
                "backgroundColor": if dark_mode {"#eee"} else {"#666"},
                "shadowColor": if dark_mode {"#999"} else {"#aaa"},
                "color": if dark_mode {"#333"} else {"#eee"}
            }
        },
        "textStyle": {
            "color": if dark_mode {"#c0c5ce"} else {"#333"}
        },
        "toolbox": {
            "bottom": 0,
            "feature": {
                "dataZoom": {
                    "yAxisIndex": "none"
                },
                "magicType": {
                    "show": true,
                    "type": [
                        "line",
                        "bar"
                    ]
                },
                "restore": {
                    "show": true
                },
                "saveAsImage": {
                    "show": true
                }
            },
            "emphasis": {
                "iconStyle": {
                    "textBackgroundColor": if dark_mode {"#1f2937"} else {"#ffffff"},
                    "textFill": if dark_mode {"#c0c5ce"} else {"#333"},
                    "textPadding": 10,
                }
            }
        },
        "legend": {
            "data": legend_vec,
            "selected": legend_disabled,
            "textStyle": {
                "color": if dark_mode {"#c0c5ce"} else {"#333"}
            },
        },
        "xAxis": [
            {
                "type": "time",
                "data": timestamps,
                "min": if timestamp_min.is_some() {json!(timestamp_min.unwrap())} else {serde_json::value::Value::Null},
                "max": if timestamp_max.is_some() {json!(timestamp_max.unwrap())} else {serde_json::value::Value::Null},
                "splitNumber": 6,
                "axisPointer": {
                    "type": "shadow"
                }
            }
        ],
        "yAxis": [
            {
                "type": "value",
                "name": "kWh",
                "nameLocation": "start",
                "nameGap": 23,
                "nameTextStyle": {
                    "align": "right"
                },
                "splitNumber": 5,
                "axisLabel": {
                    "formatter": "{value}"
                },
                "axisLine": {
                    "show": true
                }
            },
            {
                "show": allowed_fields.contains(&"battery_soc".to_string()),
                "type": "value",
                "name": "Battery SOC",
                "nameLocation": "start",
                "nameGap": 23,
                "nameTextStyle": {
                    "color": "#12a5ed"
                },
                "min": 0,
                "max": 100,
                "axisLabel": {
                    "formatter": "{value} %",
                    "color": "#12a5ed",
                    "margin": 6,
                },
                "axisLine": {
                    "show": true,
                    "lineStyle": {
                        "color": "#12a5ed"
                    }
                },
                "splitLine": false,
                "axisPointer": {
                    "show": allowed_fields.contains(&"battery_soc".to_string()),
                    "label": {
                        "backgroundColor": if dark_mode {"#32c5ff"} else {"#12a5ed"},
                        "shadowColor": if dark_mode {"#32c5ff"} else {"#12a5ed"},
                        "color": if dark_mode {"#333"} else {"#fff"},
                        "precision": 0
                    }
                }
            },
            {
                "show": allowed_fields.contains(&"plug1".to_string()) || allowed_fields.contains(&"plug2".to_string()),
                "type": "value",
                "name": "Wh",
                "offset": if !allowed_fields.contains(&"battery_soc".to_string()) {0} else {40},
                "nameLocation": "start",
                "nameGap": 23,
                "nameTextStyle": {
                    "align": "left",
                    "color": "#619c45"
                },
                "splitNumber": 5,
                "axisLabel": {
                    "formatter": "{value}",
                    "color": "#619c45",
                    "margin": 4,
                },
                "axisLine": {
                    "show": !allowed_fields.contains(&"battery_soc".to_string()),
                    "lineStyle": {
                        "color": "#619c45"
                    }
                },
                "splitLine": !allowed_fields.contains(&"consumption".to_string()) && !allowed_fields.contains(&"solar".to_string()),
                "axisPointer": {
                    "show": allowed_fields.contains(&"plug1".to_string()) || allowed_fields.contains(&"plug2".to_string()),
                    "label": {
                        "backgroundColor": if dark_mode {"#91cc75"} else {"#619c45"},
                        "shadowColor": if dark_mode {"#619c45"} else {"#619c45"},
                        "color": if dark_mode {"#333"} else {"#fff"},
                        "precision": 0
                    }
                }
            }
        ],
        "series": series_vec
    }))
    .expect("Invalid json configuration in energy_overview")
}
