use crate::api_query;
use leptos::*;
use leptos_query::QueryResult;
use sumon_common::{BranchData, Device};

#[component]
pub fn DeviceList(branch_signal: RwSignal<BranchData>) -> impl IntoView {
    let QueryResult {
        data: devices_result,
        ..
    } = api_query::use_user_branch_device_list_query(move || {
        api_query::UserBranchDevicesQueryKey(branch_signal.get().id.clone().to_string())
    });

    view! {
        <Transition fallback=move || {
            view! {
                <div>
                    <span text="Loading devices..."></span>
                </div>
            }
        }>
            {move || {
                devices_result
                    .get()
                    .map(api_query::handle_unauthorized::<Vec<Device>>)
                    .map(|devices_result| {
                        match devices_result {
                            Ok(devices) => {
                                view! {
                                    <div>
                                        <ul>
                                            <For
                                                each=move || devices.clone()
                                                key=|device| device.id
                                                children=|device| {
                                                    view! { <Device device/> }
                                                }
                                            />

                                        </ul>
                                    </div>
                                }
                            }
                            Err(_) => {
                                view! {
                                    <div>
                                        <span>Failed to load devices.</span>
                                    </div>
                                }
                            }
                        }
                    })
            }}

        </Transition>
    }
}

#[component]
pub fn Device(device: Device) -> impl IntoView {
    let device_type_name = match device.device_type_id {
        1 => "Victron Cerbo GX",
        2 => "Tasmota plug",
        _ => "unknown",
    };
    let state_indicator = match device.enabled {
        true => {
            view! { <span class="inline-block w-3 h-3 mx-2 bg-green-500 rounded-full" title="Enabled"></span> }
        }
        false => {
            view! { <span class="inline-block w-3 h-3 mx-2 bg-red-500 rounded-full" title="Disabled"></span> }
        }
    };

    view! {
        <li>
            {device_type_name} " - " <strong>{device.name}</strong> " [" {device.id.to_string()} "]"
            {state_indicator}
        </li>
    }
}
