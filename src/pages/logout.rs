use leptos::*;
// use leptos_router::*;

use crate::GlobalState;

#[component]
pub fn Logout() -> impl IntoView {
    let state_signal = expect_context::<RwSignal<GlobalState>>();
    let mut state = {move || state_signal.get()}();
    state.authenticated = false;
    state.save();
    state_signal.set(state);

    let global_state = GlobalState::load();
    leptos::logging::log!("logout GlobalState::load(): {:#?}", global_state);

    // view! { <Redirect path="/" /> }

    // use js redirect because leptos Redirect somehow causes immediate logout after login
    // use js redirect to avoid cache problems (use_query_client().invalidate_all_queries() doesn't seem to help)
    let _ = window().location().replace("/");

    // let state = expect_context::<RwSignal<GlobalState>>();
    // // `create_slice` lets us create a "lens" into the data
    // let (_authenticated, set_authenticated) = create_slice(
    //     // we take a slice *from* `state`
    //     state,
    //     // our getter returns a "slice" of the data
    //     |state| state.authenticated,
    //     // our setter describes how to mutate that slice, given a new value
    //     |state, n| state.authenticated = n,
    // );
    // set_authenticated.set(false);

    // let navigate = use_navigate();
    // // Perform the programmatic redirect with custom options
    // let result = navigate("/", NavigateOptions {
    //     resolve: false,
    //     replace: false,
    //     scroll: true,
    //     state: State(None), // Replace with your custom state
    // });
    // if let Err(error) = result {
    //     leptos::log!("Logout navigate result error: {}", error);
    // }
    // request_animation_frame(move || {
    //    let _ = navigate("", Default::default());
    // });
    // return;
    // view! {
    //     <h1>Logout</h1>
    // }
}