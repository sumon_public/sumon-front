use crate::pages::branch::branch_list::BranchList;
use leptos::*;
use leptos_meta::*;

#[component]
pub fn Home() -> impl IntoView {
    view! {
        <Title text="Home | Sumon"/>
        <div class="container mx-auto max-w-screen-xl p-4">
            <h1 class="text-4xl mt-3 mb-0 pb-3 text-gray-700 dark:text-gray-400 border-b border-gray-200 dark:border-gray-700">
                List of available branches
            </h1>
            <BranchList/>
        </div>
    }
}
