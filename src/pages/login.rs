use crate::{api::{self, RequestResult}, components::loader::SpinningLoader, GlobalState};
use leptos::{*, ev::{SubmitEvent, CustomEvent}, html::Div};
use leptos_meta::*;
// use leptos_query::*;
use load_dotenv::load_dotenv;
// use wasm_bindgen::prelude::*;
use serde::Deserialize;
use gloo_utils::format::JsValueSerdeExt;

load_dotenv!(); // load environment variables from .env file

#[component]
pub fn Login() -> impl IntoView {
    use leptos::html::Input;

    let google_client_id = std::env!("GOOGLE_CLIENT_ID");

    // signal for error message text
    let (error_message_text, set_error_message_text) = create_signal("".to_string());

    // signal for animated loader
    let (show_animated_loader, set_show_animated_loader) = create_signal(false);

    // we'll use a NodeRef to store a reference to the input element
    // this will be filled when the element is created
    let email_element: NodeRef<Input> = create_node_ref();
    let password_element: NodeRef<Input> = create_node_ref();

    // fires when the form `submit` event happens
    // this will store the value of the <input> in our signal
    let on_submit = move |ev: SubmitEvent| {
        // stop the page from reloading!
        ev.prevent_default();

        // here, we'll extract the value from the input
        let email = email_element.get()
            // event handlers can only fire after the view
            // is mounted to the DOM, so the `NodeRef` will be `Some`
            .expect("<input> to exist")
            // `NodeRef` implements `Deref` for the DOM element type
            // this means we can call`HtmlInputElement::value()`
            // to get the current value of the input
            .value();

        let password = password_element.get().expect("<input> to exist").value();


        // Use spawn_local to execute asynchronous code
        set_show_animated_loader.set(true);
        // let navigate = leptos_router::use_navigate(); // prepare navigate so we can redirect from the async block
        spawn_local(async move {
            match api::login_with_credentials(email, password).await {
                Ok(message) => {
                    leptos::logging::log!("login_with_credentials OK response: {}", message);

                    let user_data = match api::get_user("authenticated_user".to_string()).await {
                        RequestResult::Ok(user_data) => Some(user_data),
                        _ => None,
                    };

                    let state_signal = expect_context::<RwSignal<GlobalState>>();
                    let mut state = state_signal.get_untracked();
                    state.authenticated = true;
                    state.user = user_data;
                    state.save();

                    // clear cache before state_signal.set()
                    // let _ = use_query_client().invalidate_all_queries(); // clear query cache
                    // leptos::logging::log!("Query cache invalidated!");
                    // let query_client = use_query_client();
                    // leptos::logging::log!("leptos_query cache size: {}", query_client.size().get_untracked());

                    // leptos::logging::log!("login.rs: before state_signal.set(state)");
                    // state_signal.set(state);
                    // let _ = navigate("/home", Default::default());
                    // use js redirect to avoid cache problems (use_query_client().invalidate_all_queries() doesn't seem to help)
                    let _ = window().location().replace("/home");
                },
                Err(message) => {
                    set_error_message_text.set(message);
                    set_show_animated_loader.set(false);
                },
            };
        });
    };

    let google_btn: NodeRef<Div> = create_node_ref();
    let on_my_google_login_event = move |ev: CustomEvent| {
        ev.stop_immediate_propagation();
        #[derive(Deserialize)]
        struct GoogleCustomEventDetail {
            client_id: Option<String>, // client_id is not present when using FedCM prompt
            token: String,
        }
        let ev_detail: GoogleCustomEventDetail = ev.detail().into_serde().unwrap();
        match &ev_detail.client_id {
            Some(client_id) => leptos::logging::log!("leptos log: attempt to log in using google with client_id: {}", client_id),
            None => leptos::logging::log!("leptos log: attempt to log in using google FedCM (no client_id present)"),
        }

        // Use spawn_local to execute asynchronous code
        set_show_animated_loader.set(true);
        // let navigate = leptos_router::use_navigate(); // prepare navigate so we can redirect from the async block
        spawn_local(async move {
            match api::login_with_google(ev_detail.token).await {
                Ok(_) => {
                    leptos::logging::log!("leptos log: api login-google OK!");

                    let user_data = match api::get_user("authenticated_user".to_string()).await {
                        RequestResult::Ok(user_data) => Some(user_data),
                        _ => None,
                    };

                    let state_signal = expect_context::<RwSignal<GlobalState>>();
                    let mut state = state_signal.get_untracked();
                    state.authenticated = true;
                    state.user = user_data;
                    state.save();

                    // clear cache before state_signal.set()
                    // let _ = use_query_client().invalidate_all_queries(); // clear query cache
                    // leptos::logging::log!("Query cache invalidated!");
                    // let query_client = use_query_client();
                    // leptos::logging::log!("leptos_query cache size: {}", query_client.size().get_untracked());

                    // leptos::logging::log!("login.rs: before state_signal.set(state)");
                    // state_signal.set(state);
                    // let _ = navigate("/home", Default::default());
                    // use js redirect to avoid cache problems (use_query_client().invalidate_all_queries() doesn't seem to help)
                    let _ = window().location().replace("/home");
                },
                Err(message) => {
                    set_error_message_text.set(format!("Google login failed: {}", message));
                    set_show_animated_loader.set(false);
                },
            };
        });
    };

    view! {
        <Title text="Login | Sumon"/>
        <form on:submit=on_submit>
            <div class="flex items-center">
                <div class="h-max mx-auto pb-[4rem] tall:mb-[15vh] tall-extra:mb-[25vh] flex flex-col items-center">
                    // <img class="h-[40px] w-[47px] mb-5" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt="" />
                    <img
                        class="h-[75px] w-[75px] tall:h-[100px] tall:w-[100px] mt-[2rem] tall:mt-[4rem] mb-5"
                        src="/images/logo.svg"
                        alt="Sumon logo"
                    />
                    <h1 class="text-xl font-bold text-center pb-4 tall:pb-8 dark:text-slate-300">
                        Sumon
                    </h1>
                    <div class="bg-white border rounded-md dark:bg-slate-800 dark:border-slate-600 shadow-xl p-10 flex flex-col gap-4 text-sm">
                        <div
                            class="border-l-4 border-rose-500 bg-red-100 hidden text-rose-700 p-2 dark:bg-rose-950 dark:border-rose-700 dark:text-rose-200"
                            role="alert"
                            class:hidden=move || error_message_text.get().is_empty()
                        >
                            <p class="font-bold">{move || error_message_text.get()}</p>
                        </div>
                        <div>
                            <label
                                class="text-gray-600 dark:text-gray-400 font-bold inline-block pb-2"
                                for="input-email"
                            >
                                Email
                            </label>
                            <input
                                node_ref=email_element
                                type="email"
                                name="email"
                                id="input-email"
                                autocomplete="email"
                                placeholder="email@somewhere.com"
                                required
                                class="border border-gray-400 focus:outline-slate-400 rounded-md w-full shadow-sm px-3 py-2 dark:bg-gray-800 dark:border-gray-500 dark:focus:outline-slate-800 dark:text-white"
                                class:cursor-not-allowed=move || show_animated_loader.get()
                            />
                        </div>
                        <div>
                            <label
                                class="text-gray-600 dark:text-gray-400 font-bold inline-block pb-2"
                                for="input-password"
                            >
                                Password
                            </label>
                            <input
                                node_ref=password_element
                                type="password"
                                name="password"
                                id="input-password"
                                placeholder="******"
                                required
                                class="border border-gray-400 focus:outline-slate-400 rounded-md w-full shadow-sm px-3 py-2 dark:bg-gray-800 dark:border-gray-500 dark:focus:outline-slate-800 dark:text-white"
                                class:cursor-not-allowed=move || show_animated_loader.get()
                            />
                        </div>
                        // <div class="flex">
                        // <div class="w-1/2">
                        // <input type="checkbox" name="remeberMe"/>
                        // <label for="remeberMe">Remeber me</label>
                        // </div>
                        // <div class="w-1/2">
                        // <a class="font-bold text-blue-600" href="">Forgot password ?</a>
                        // </div>
                        // </div>
                        <div>
                            <button
                                type="submit"
                                value="Login"
                                // class="bg-[#4F46E5] w-full py-2 rounded-md text-white font-bold cursor-pointer hover:bg-[#181196]">
                                class="relative items-center w-full px-4 py-2 font-semibold leading-6 text-sm text-center shadow rounded-md text-white bg-[#4F46E5] hover:bg-[#181196] dark:bg-[#3730A3] dark:hover:bg-[#4F46E5] dark:text-slate-200"
                                class:cursor-progress=move || show_animated_loader.get()
                                disabled=move || show_animated_loader.get()
                            >
                                <span class="absolute top-1/4 left-4 -ml-1 mr-3 h-5 w-5 text-white">
                                    <SpinningLoader show=Some(show_animated_loader)/>
                                </span>
                                <span>Login</span>
                            </button>
                        </div>
                        <div>
                            <p class="text-center">Or continue with</p>
                        </div>
                        <div class="flex gap-4">
                            <div
                                node_ref=google_btn
                                on:myGoogleLoginEvent=on_my_google_login_event
                                id="googleLoginButton"
                                data-my-google-client-id=google_client_id
                                style="width:100%;"
                            ></div>
                            <script>googleInitButton();</script>
                        </div>
                    </div>
                // <p class="text-sm text-gray-500 mt-10">Not a member? <a href="#" class="text-[#4F46E5] font-bold">Start a 14 day free trial</a></p>
                </div>
            </div>
        </form>
    }
}

// #[wasm_bindgen]
// pub fn handle_google_login(client_id: String, credential: String) {
//     leptos::log!("leptos log: Rust callback: Google login successful!");
//     // You can perform any Rust-specific actions here
//     alert(&format!(
//         "Hello {} from rust handle_google_login(): {}",
//         client_id, credential
//     ));
// }