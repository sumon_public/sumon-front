use crate::components::alert::Alert;
use crate::{api_query, components::loader::SpinningLoader};
use leptos::*;
use leptos_query::QueryResult;
use sumon_common::{BranchData, BranchState};

#[component]
pub fn BranchList() -> impl IntoView {
    let QueryResult {
        data: user_branch_list_result,
        ..
    } = api_query::use_user_branch_list_query(move || "authenticated_user_branch_list".to_string());

    view! {
        <Transition fallback=move || {
            view! {
                <div class="w-[10%] max-w-[100px] my-10 mt-[115px] mx-auto align-middle text-gray-500 dark:text-gray">
                    <SpinningLoader show=None/>
                </div>
            }
        }>
            {move || {
                user_branch_list_result
                    .get()
                    .map(api_query::handle_unauthorized::<Vec<BranchData>>)
                    .map(|user_branch_list_result| {
                        match user_branch_list_result {
                            Ok(user_branch_list) => {
                                view! {
                                    <div class="mt-4 grid w-full gap-6 md:grid-cols-2 xl:grid-cols-3">
                                        {user_branch_list
                                            .into_iter()
                                            .map(|n| view! { <BranchListItem branch=n/> })
                                            .collect_view()}
                                    </div>
                                }
                            }
                            Err(e) => {
                                view! {
                                    <div>
                                        <Alert message=format!("Failed to load branches: {}", e)/>
                                    </div>
                                }
                            }
                        }
                    })
            }}

        </Transition>
    }
}

#[component]
pub fn BranchListItem(branch: BranchData) -> impl IntoView {
    let (branch_state_read, _) = create_signal(branch.state.clone());
    view! {
        <a
            href=format!("branch/{}", branch.id)
            class="block p-5 py-3 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700"
        >
            <h5
                class="mb-2 pb-2 text-2xl font-bold tracking-tight text-gray-700 dark:text-gray-300"
            >
                <span
                    class="inline-block w-3 h-3 ml-0 mr-4 bg-green-500 rounded-full"
                    class:bg-yellow-500=move || branch_state_read.get().eq(&BranchState::New)
                    class:bg-green-500=move || branch_state_read.get().eq(&BranchState::Online)
                    class:bg-red-500=move || branch_state_read.get().eq(&BranchState::Offline)
                    title={move || format!("{:?}", branch_state_read.get())}
                ></span>
                {branch.name}
            </h5>

            <p class="font-normal text-gray-700 dark:text-gray-400" title={move || {
                if let Some(last_connection) = branch.last_connection {
                    format!("Last connection {}", last_connection.format("%d.%m.%Y %H:%M:%S").to_string())
                } else {
                    "Never connected".to_string()
                }
            }}>
                {move || format!("{:?}", branch_state_read.get())}
            </p>
        </a>
    }
}
