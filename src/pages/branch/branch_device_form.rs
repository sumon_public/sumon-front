use leptos::*;
// use leptos_meta::*;
use leptos_query::*;
use serde::{Deserialize, Serialize};
use sumon_common::Device;
use uuid::Uuid;
use wasm_bindgen::JsCast;
use web_sys::{HtmlInputElement, HtmlSelectElement, HtmlTextAreaElement};

use crate::{api::{self, RequestResult}, api_query::{self, UserBranchDevicesQueryKey}};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum DeviceFormState {
    Hidden,
    New,
    Some(Device),
}

#[allow(dead_code)]
impl DeviceFormState {
    fn is_some(&self) -> bool {
        match *self {
            DeviceFormState::Some(_) => true,
            _ => false,
        }
    }
    fn is_new(&self) -> bool {
        match *self {
            DeviceFormState::New => true,
            _ => false,
        }
    }
    fn is_hidden(&self) -> bool {
        match *self {
            DeviceFormState::Hidden => true,
            _ => false,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct NewDevice {
    id: Option<Uuid>,
    name: String,
    label: Option<String>,
    device_type_id: i32,
    mqtt_topic: Option<String>,
    config: Option<String>,
    enabled: bool,
}

#[component]
pub fn DeviceEntryForm(branch_id: Uuid, device_form_signal: RwSignal<DeviceFormState>) -> impl IntoView {
    let id = create_rw_signal(String::new());
    let id_submit = create_rw_signal(String::new());
    let name = create_rw_signal(String::new());
    let name_submit = create_rw_signal(String::new());
    let label = create_rw_signal(String::new());
    let label_submit = create_rw_signal(String::new());
    let device_type_id = create_rw_signal(2); // Default to Tasmota plug
    let device_type_id_submit = create_rw_signal(2); // Default to Tasmota plug
    let mqtt_topic = create_rw_signal(String::new());
    let mqtt_topic_submit = create_rw_signal(String::new());
    let config = create_rw_signal(String::new());
    let config_submit = create_rw_signal(String::new());
    let enabled = create_rw_signal(false);
    let enabled_submit = create_rw_signal(false);

    create_effect(move |_| {
        match device_form_signal.get() {
            DeviceFormState::Some(device) => {
                id.set(device.id.to_string());
                id_submit.set(device.id.to_string());
                name.set(device.name.clone());
                name_submit.set(device.name.clone());
                label.set(device.label.clone().unwrap_or_default());
                label_submit.set(device.label.clone().unwrap_or_default());
                device_type_id.set(device.device_type_id);
                device_type_id_submit.set(device.device_type_id);
                mqtt_topic.set(device.mqtt_topic.clone().unwrap_or_default());
                mqtt_topic_submit.set(device.mqtt_topic.clone().unwrap_or_default());
                config.set(device.config.clone().unwrap_or_default());
                config_submit.set(device.config.clone().unwrap_or_default());
                enabled.set(device.enabled);
                enabled_submit.set(device.enabled);
            },
            DeviceFormState::New => {
                id.set(String::new());
                id_submit.set(String::new());
                name.set(String::new());
                name_submit.set(String::new());
                label.set(String::new());
                label_submit.set(String::new());
                device_type_id.set(2); // Default to Tasmota plug
                device_type_id_submit.set(2); // Default to Tasmota plug
                mqtt_topic.set(String::new());
                mqtt_topic_submit.set(String::new());
                config.set(String::new());
                config_submit.set(String::new());
                enabled.set(false);
                enabled_submit.set(false);
            },
            _ => {},
        };
    });

    let on_submit = move |evt: leptos::ev::SubmitEvent| {
        evt.prevent_default();

        // let config_default = match device_type_id.get() {
        //     1 => Some(r##"{"mqtt_host":"10.0.0.68","mqtt_port":1883,"mqtt_topics_prefix":"N/c0619ab3ab55/","mqtt_topics":{"system/0/Dc/Pv/Power":"PvPower","vebus/276/Ac/Out/P":"AcConsumptionPower","vebus/276/Ac/ActiveIn/P":"AcGridPower","system/0/Dc/Vebus/Power":"DcOwnConsumption","system/0/Dc/Battery/Soc":"BatterySoc","system/0/Dc/Battery/Voltage":"BatteryVoltage","system/0/Dc/Battery/Current":"BatteryCurrent","system/0/Dc/Battery/Power":"BatteryPower","system/0/Dc/Battery/Temperature":"BatteryTemperature","battery/512/Soh":"BatteryHealth","battery/512/System/MaxCellTemperature":"BatteryMaxCellTemp","battery/512/System/MinCellTemperature":"BatteryMinCellTemp","battery/512/System/MinCellVoltage":"BatteryMinCellVoltage","battery/512/System/MaxCellVoltage":"BatteryMaxCellVoltage","system/0/Ac/ConsumptionOnOutput/L1/Current":"AcConsumptionCurrentL1","system/0/Ac/ConsumptionOnOutput/L1/Power":"AcConsumptionPowerL1","system/0/Ac/ConsumptionOnOutput/L2/Current":"AcConsumptionCurrentL2","system/0/Ac/ConsumptionOnOutput/L2/Power":"AcConsumptionPowerL2","system/0/Ac/ConsumptionOnOutput/L3/Current":"AcConsumptionCurrentL3","system/0/Ac/ConsumptionOnOutput/L3/Power":"AcConsumptionPowerL3","system/0/Ac/Grid/L1/Current":"AcGridCurrentL1","system/0/Ac/Grid/L1/Power":"AcGridPowerL1","system/0/Ac/Grid/L2/Current":"AcGridCurrentL2","system/0/Ac/Grid/L2/Power":"AcGridPowerL2","system/0/Ac/Grid/L3/Current":"AcGridCurrentL3","system/0/Ac/Grid/L3/Power":"AcGridPowerL3","vebus/276/Ac/ActiveIn/L1/V":"AcGridVoltageL1","vebus/276/Ac/ActiveIn/L2/V":"AcGridVoltageL2","vebus/276/Ac/ActiveIn/L3/V":"AcGridVoltageL3","vebus/276/Ac/Out/L1/V":"AcOutVoltageL1","vebus/276/Ac/Out/L2/V":"AcOutVoltageL2","vebus/276/Ac/Out/L3/V":"AcOutVoltageL3","solarcharger/0/Yield/Power":"PvPower0","solarcharger/0/Pv/V":"PvVoltage0","solarcharger/0/Dc/0/Voltage":"PvDcVoltage0","solarcharger/0/Dc/0/Current":"PvDcCurrent0","solarcharger/1/Yield/Power":"PvPower1","solarcharger/1/Pv/V":"PvVoltage1","solarcharger/1/Dc/0/Voltage":"PvDcVoltage1","solarcharger/1/Dc/0/Current":"PvDcCurrent1"},"mqtt_keepalive_topic":"R/c0619ab3ab55/keepalive"}"##.to_string()),
        //     _ => None,
        // };
        let config_raw = config_submit.get();

        let new_device = Device {
            id: if id_submit.get().is_empty() {
                Uuid::new_v4()
            } else {
                Uuid::parse_str(&id_submit.get()).unwrap_or_else(|_| Uuid::new_v4())
            },
            name: name_submit.get().to_string(),
            label: if label_submit.get().is_empty() { None } else { Some(label_submit.get().to_string()) },
            device_type_id: device_type_id_submit.get(),
            mqtt_topic: if mqtt_topic_submit.get().is_empty() { None } else { Some(mqtt_topic_submit.get().to_string()) },
            config: if config_raw.is_empty() {None} else {Some(config_raw)},
            enabled: enabled_submit.get(),
        };

        spawn_local(async move {
            let params = api::LocalDeviceSetParams {
                branch_id: branch_id.clone().to_string(),
                device: new_device.clone(),
            };
            let request_result = api::set_local_device(params).await;
            match api_query::handle_unauthorized::<Device>(request_result) {
                Ok(device_data) => {
                    leptos::logging::log!("leptos log: api set_local_device OK: {:?}", &device_data);
                    let client = use_query_client();
                    client.invalidate_query::<UserBranchDevicesQueryKey, RequestResult<Vec<Device>>>(UserBranchDevicesQueryKey(branch_id.clone().to_string()));
                },
                Err(message) => {
                    leptos::logging::log!("set_local_device() failed: {}", message);
                },
            };
        });
    };

    let on_remove = move |device_id: Uuid| {
        spawn_local(async move {
            let params = api::LocalDeviceRemoveParams {
                branch_id: branch_id.clone().to_string(),
                device_id,
            };
            let request_result = api::remove_local_device(params).await;
            match api_query::handle_unauthorized::<Device>(request_result) {
                Ok(device_data) => {
                    leptos::logging::log!("leptos log: api remove_local_device() OK: {:?}", &device_data);
                    let client = use_query_client();
                    client.invalidate_query::<UserBranchDevicesQueryKey, RequestResult<Vec<Device>>>(UserBranchDevicesQueryKey(branch_id.clone().to_string()));
                },
                Err(message) => {
                    leptos::logging::log!("remove_local_device() failed: {}", message);
                },
            };
        });
    };

    view! {
        // seet overflow:hidden on body element to avoid double scroll with modal
        <Show when=move || !device_form_signal.get().is_hidden() fallback=|| {
            let document = web_sys::window().unwrap().document().unwrap();
            let body = document.body().unwrap();
            body.class_list().remove_1("overflow-hidden").unwrap();
        }>
            {
                let document = web_sys::window().unwrap().document().unwrap();
                let body = document.body().unwrap();
                body.class_list().add_1("overflow-hidden").unwrap();
            }
            // <Body class="bg-[#F9FAFB] dark:bg-[#111827] dark:text-slate-300 overflow-hidden"/>
        </Show>

        <button
            class="block text-white bg-[#4F46E5] hover:bg-[#181196] dark:bg-[#3730A3] dark:hover:bg-[#4F46E5] dark:text-slate-200 focus:ring-4 focus:outline-none focus:ring-blue-300 font-bold rounded-lg text-sm px-5 py-2.5 text-center"
            type="button"
            on:click=move |_| device_form_signal.set(DeviceFormState::New)
        >
            Add new device
        </button>

        <div
            id="default-modal"
            tabindex="-1"
            aria-hidden="true"
            class="fixed top-0 left-0 right-0 z-50 w-full -mb-4 p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[100%] max-h-full bg-black/50"
            class:hidden=move || device_form_signal.get().is_hidden()
        >
            <div class="relative w-full max-w-2xl max-h-full mx-auto pb-6">
                //<!-- Modal content -->
                <div class="relative mb-6 border rounded-lg shadow dark:bg-gray-700 bg-white dark:bg-slate-800 dark:border-slate-600">
                    //<!-- Modal header -->
                    <div class="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
                        <h3 class="text-xl font-semibold text-gray-900 dark:text-white">
                            {move || if device_form_signal.get().is_some() {
                                view! { Device settings }
                            } else { view! { New device } }}
                        </h3>
                        <button
                            type="button"
                            class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex items-center justify-center dark:hover:bg-gray-600 dark:hover:text-white"
                            title="Close form"
                            on:click=move |_| device_form_signal.set(DeviceFormState::Hidden)
                        >
                            <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 1l12 12M1 13L13 1"/>
                            </svg>
                            <span class="sr-only">Close modal</span>
                        </button>
                    </div>
                    //<!-- Modal body -->
                    <div class="p-6 space-y-6">
                        // class="max-w-lg mx-auto p-4"
                        <form on:submit=on_submit>
                            <div class="mb-4">
                                <label class="block text-gray-700 dark:text-gray-300 text-sm font-bold mb-2" for="deviceFormId">ID</label>
                                <input
                                    class="shadow appearance-none border border-gray-400 dark:border-gray-600 rounded w-full py-2 px-3 text-gray-700 dark:bg-gray-800 dark:text-gray-300 leading-tight focus:outline-none focus:shadow-outline dark:placeholder:text-gray-600"
                                    id="deviceFormId"
                                    type="text"
                                    placeholder="Device UUID (optional)"
                                    pattern="^$|^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$" 
                                    title="Please enter a valid UUID or leave the field empty"
                                    prop:value=move || id.get().clone()
                                    readonly=move || device_form_signal.get().is_some()
                                    on:input=move |e| {
                                        let input = e.target().unwrap().dyn_into::<HtmlInputElement>().unwrap();
                                        id_submit.set(input.value());
                                    }
                                />
                                <Show when=move || !device_form_signal.get().is_some()>
                                    <p id="helper-text-explanation" class="mt-2 text-sm text-gray-500 dark:text-gray-400">When empty UUID will be generated automatically.</p>
                                </Show>
                            </div>
                            <div class="mb-4">
                                <label class="block text-gray-700 dark:text-gray-300 text-sm font-bold mb-2" for="deviceFormName">Name</label>
                                <input
                                    class="shadow appearance-none border border-gray-400 dark:border-gray-600 rounded w-full py-2 px-3 text-gray-700 dark:bg-gray-800 dark:text-gray-300 leading-tight focus:outline-none focus:shadow-outline dark:placeholder:text-gray-600"
                                    id="deviceFormName"
                                    type="text"
                                    placeholder="Smart plug 1"
                                    required
                                    pattern="^(?!\\s*$).+" 
                                    title="Please enter a non-empty string with alphanumeric characters"
                                    prop:value=move || name.get().clone()
                                    on:input=move |e| {
                                        let input = e.target().unwrap().dyn_into::<HtmlInputElement>().unwrap();
                                        name_submit.set(input.value());
                                    }
                                />
                                <p id="helper-text-explanation" class="mt-2 text-sm text-gray-500 dark:text-gray-400">Mandatory device name.</p>
                            </div>
                            <div class="mb-4">
                                <label class="block text-gray-700 dark:text-gray-300 text-sm font-bold mb-2" for="deviceFormLabel">Label</label>
                                <input
                                    class="shadow appearance-none border border-gray-400 dark:border-gray-600 rounded w-full py-2 px-3 text-gray-700 dark:bg-gray-800 dark:text-gray-300 leading-tight focus:outline-none focus:shadow-outline dark:placeholder:text-gray-600"
                                    id="deviceFormLabel"
                                    type="text"
                                    placeholder="Living room"
                                    pattern="^(?!\\s*$).+" 
                                    title="Please enter a string with alphanumeric characters or leave empty"
                                    prop:value=move || label.get().clone()
                                    on:input=move |e| {
                                        let input = e.target().unwrap().dyn_into::<HtmlInputElement>().unwrap();
                                        label_submit.set(input.value());
                                    }
                                />
                                <p id="helper-text-explanation" class="mt-2 text-sm text-gray-500 dark:text-gray-400">Optional temporary label of the device that is stored together with collected timeseries data. " " A single device might have multiple labels throughout the history.</p>
                            </div>
                            <div class="mb-4">
                                <label class="block text-gray-700 dark:text-gray-300 text-sm font-bold mb-2" for="deviceFormDevice_type_id">Device Type</label>
                                <select
                                    class="block w-full bg-white dark:bg-gray-800 border border-gray-400 dark:border-gray-600 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline cursor-pointer"
                                    id="deviceFormDevice_type_id"
                                    // value=move || device_type_id.get().to_string()
                                    prop:value=move || device_type_id.get().to_string()
                                    required
                                    on:change=move |e| {
                                        let select = e.target().unwrap().dyn_into::<HtmlSelectElement>().unwrap();
                                        device_type_id_submit.set(select.value().parse().unwrap_or(2));
                                    }
                                >
                                    <option value="1" selected={move || device_type_id.get() == 1}>Victron Cerbo GX</option>
                                    <option value="2" selected={move || device_type_id.get() == 2}>Tasmota plug</option>
                                </select>
                            </div>
                            <div class="mb-4">
                                <label class="block text-gray-700 dark:text-gray-300 text-sm font-bold mb-2" for="deviceFormMqtt_topic">Optional MQTT Topic</label>
                                <input
                                    class="shadow appearance-none border border-gray-400 dark:border-gray-600 rounded w-full py-2 px-3 text-gray-700 dark:bg-gray-800 dark:text-gray-300 leading-tight focus:outline-none focus:shadow-outline dark:placeholder:text-gray-600"
                                    id="deviceFormMqtt_topic"
                                    type="text"
                                    placeholder="tele/plug/tasmota_AA00A0/SENSOR"
                                    prop:value=move || mqtt_topic.get().clone()
                                    on:input=move |e| {
                                        let input = e.target().unwrap().dyn_into::<HtmlInputElement>().unwrap();
                                        mqtt_topic_submit.set(input.value());
                                    }
                                />
                            </div>
                            <div class="mb-4">
                                <label class="block text-gray-700 dark:text-gray-300 text-sm font-bold mb-2" for="deviceFormConfig">Optional config</label>
                                <textarea
                                    class="shadow appearance-none border border-gray-400 dark:border-gray-600 rounded w-full py-2 px-3 text-gray-700 dark:bg-gray-800 dark:text-gray-300 leading-tight focus:outline-none focus:shadow-outline dark:placeholder:text-gray-600"
                                    id="deviceFormConfig"
                                    type="text"
                                    placeholder="{}"
                                    prop:value=move || config.get().clone()
                                    on:input=move |e| {
                                        leptos::logging::log!("textarea input change");
                                        let input = e.target().unwrap().dyn_into::<HtmlTextAreaElement>().unwrap();
                                        leptos::logging::log!("textarea input change: {}", input.value());
                                        config_submit.set(input.value());
                                    }
                                >{move || config.get().clone()}</textarea>
                            </div>
                            <div class="mb-4">
                                <label class="inline-block text-gray-700 dark:text-gray-300 text-sm font-bold mb-2 mr-2 cursor-pointer" for="enabled">Enabled</label>
                                <input
                                    type="checkbox"
                                    id="enabled"
                                    prop:checked=move || enabled.get()
                                    class="cursor-pointer"
                                    on:change=move |e| {
                                        let input = e.target().unwrap().dyn_into::<HtmlInputElement>().unwrap();
                                        enabled_submit.set(input.checked());
                                    }
                                />
                            </div>
                            <div class="flex items-center justify-between">
                                <button
                                    class="text-white bg-[#4F46E5] hover:bg-[#181196] dark:bg-[#3730A3] dark:hover:bg-[#4F46E5] dark:text-slate-200 font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                    type="submit"
                                >
                                    {move || if device_form_signal.get().is_some() {
                                        view! { Save configuration }
                                    } else { view! { Save device } }}
                                </button>
                                {move || if device_form_signal.get().is_some() {
                                    view! {
                                        <><button
                                            class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                            type="button"
                                            on:click=move |_| {
                                                if web_sys::window().unwrap().confirm_with_message("Are you sure you want to remove this device?").unwrap() {
                                                    if let Ok(id) = Uuid::parse_str(&id.get()) {
                                                        on_remove(id);
                                                    }
                                                }
                                            }
                                        >
                                            <svg class="hidden md:inline-block h-5 w-5 -mt-1 mr-2 fill-white" id="icon_trash_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="109.484px" height="122.88px" viewBox="0 0 109.484 122.88" enable-background="new 0 0 109.484 122.88" xml:space="preserve"><g><path fill-rule="evenodd" clip-rule="evenodd" d="M2.347,9.633h38.297V3.76c0-2.068,1.689-3.76,3.76-3.76h21.144 c2.07,0,3.76,1.691,3.76,3.76v5.874h37.83c1.293,0,2.347,1.057,2.347,2.349v11.514H0V11.982C0,10.69,1.055,9.633,2.347,9.633 L2.347,9.633z M8.69,29.605h92.921c1.937,0,3.696,1.599,3.521,3.524l-7.864,86.229c-0.174,1.926-1.59,3.521-3.523,3.521h-77.3 c-1.934,0-3.352-1.592-3.524-3.521L5.166,33.129C4.994,31.197,6.751,29.605,8.69,29.605L8.69,29.605z M69.077,42.998h9.866v65.314 h-9.866V42.998L69.077,42.998z M30.072,42.998h9.867v65.314h-9.867V42.998L30.072,42.998z M49.572,42.998h9.869v65.314h-9.869 V42.998L49.572,42.998z"/></g></svg>
                                            Remove device
                                        </button></>
                                    }
                                } else { view! { <><button class="hidden"></button></> } }}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    }
}