use std::time::Duration;

use crate::api_query::{handle_unauthorized, UserBranchQueryKey};
use crate::components::alert::{Alert, Warn};
use crate::pages::branch::branch_devices::BranchDevices;
use crate::widgets::energy_overview_js::{self, EnergyOverviewField};
use crate::{api_query, components::loader::SpinningLoader};
use chrono::{Days, Local};
use leptos::*;
use leptos_meta::*;
use leptos_query::{use_query_client, QueryResult};
use leptos_router::*;
use serde::{Deserialize, Serialize};
use sumon_common::{BranchData, BranchState, Device};
use uuid::Uuid;

#[derive(Debug, Clone, PartialEq)]
pub enum BranchLoadingState {
    Loading,
    Online,
    Offline,
    Error(String),
}

#[component]
pub fn BranchDetail() -> impl IntoView {
    let handle_error = |_| {
        view! { <Redirect path="/home"/> }
    };

    let params = use_params_map();
    let id = move || {
        params.with(|params| {
            let string_id = params.get("id").cloned().unwrap_or_default();
            Uuid::parse_str(&string_id).map_err(handle_error).unwrap()
        })
    };

    let (branch_read, branch_write) = create_signal(None::<BranchData>);
    let (branch_devices_read, branch_devices_write) = create_signal(None::<Vec<Device>>);
    let (loading, loading_write) = create_signal(BranchLoadingState::Loading); // Signal to track loading state

    let QueryResult {
        data: branch_result,
        ..
    } = api_query::use_user_branch_query(move || UserBranchQueryKey(id().to_string()));

    let QueryResult {
        data: devices_result,
        ..
    } = api_query::use_user_branch_device_list_query(move || api_query::UserBranchDevicesQueryKey(id().to_string()));

    // Transition actually renders its children even before data is loaded which causes panics further down
    // So create effect to only render children when the data is actually loaded
    create_effect(move |_| {
        let retry_closure = move || {
            set_timeout(
                move || {
                    if !loading.get().eq(&BranchLoadingState::Online) {
                        // force browser reload
                        // web_sys::window().unwrap().location().reload();
                        let client = use_query_client();
                        loading_write.set(BranchLoadingState::Loading);
                        client.invalidate_all_queries();
                        // client.invalidate_query::<UserBranchQueryKey, RequestResult<BranchData>>(
                        //     UserBranchQueryKey(id().clone().to_string()),
                        // );
                    }
                },
                Duration::from_secs(10),
            );
        };
        let maybe_branch = branch_result.get();
        if let Some(branch_response) = maybe_branch {
            match handle_unauthorized::<BranchData>(branch_response.clone()) {
                Ok(branch) => {
                    branch_write.set(Some(branch.clone()));
                    if branch.state.eq(&BranchState::Online) {
                        let maybe_devices = devices_result.get();
                        if let Some(devices_response) = maybe_devices {
                            match handle_unauthorized::<Vec<Device>>(devices_response.clone()) {
                                Ok(devices) => {
                                    branch_devices_write.set(Some(devices));
                                    loading_write.set(BranchLoadingState::Online);
                                    // Data is loaded
                                }
                                Err(e) => {
                                    loading_write.set(BranchLoadingState::Error(e));
                                    // retry_closure();
                                }
                            }
                        }
                    } else {
                        loading_write.set(BranchLoadingState::Offline);
                        retry_closure();
                    }
                }
                Err(e) => {
                    loading_write.set(BranchLoadingState::Error(e));
                    retry_closure();
                }
            }
        }
    });

    view! {
        {move || {
            match loading.get() {
                BranchLoadingState::Loading => {
                    view! {
                        <>
                            <div class="w-[10%] max-w-[100px] my-10 mt-[115px] mx-auto align-middle text-gray-500 dark:text-gray">
                                <SpinningLoader show=None/>
                            </div>
                        </>
                    }
                }
                BranchLoadingState::Offline => {
                    let branch = branch_read.get().unwrap();
                    view! {
                        <>
                            <Title text=format!("{} | Sumon", branch.name.clone())/>
                            <BranchHeading branch=branch/>
                            <div class="container mx-auto max-w-screen-xl p-4 py-6">
                                <Warn message="Unable to access data. Please try again when the remote branch is online."
                                    .to_string()/>
                            </div>
                        </>
                    }
                }
                BranchLoadingState::Error(e) => {
                    let branch = branch_read.get().unwrap();
                    view! {
                        <>
                            <Title text=format!("{} | Sumon", branch.name.clone())/>
                            <BranchHeading branch=branch/>
                            <div class="container mx-auto max-w-screen-xl p-4 py-6">
                                <Alert message=format!("Failed to load data: {}", e)/>
                            </div>
                        </>
                    }
                }
                BranchLoadingState::Online => {
                    let branch = branch_read.get().unwrap();
                    let branch_signal = create_rw_signal(branch.clone());
                    provide_context(branch_signal);
                    let branch_devices = branch_devices_read.get().unwrap();
                    view! {
                        <>
                            <Title text=format!("{} | Sumon", branch.name.clone())/>
                            <BranchHeading branch=branch/>
                            <BranchCharts
                                branch=branch_signal.get()
                                branch_devices=branch_devices.clone()
                            />
                            <BranchDevices
                                branch_signal=branch_signal
                                branch_devices=branch_devices.clone()
                            />
                        </>
                    }
                }
            }
        }}
    }
}

#[component]
pub fn BranchHeading(branch: BranchData) -> impl IntoView {
    let (branch_state_read, _) = create_signal(branch.state.clone());
    view! {
        <div class="container mx-auto max-w-screen-xl p-4 pb-0">
            <h1 class="text-4xl mt-3 -mb-[1px] pb-3 text-gray-700 dark:text-gray-400 border-b border-gray-200 dark:border-gray-700">
                {branch.name}
                <span
                    class="inline-block w-5 h-5 ml-5 mr-0 mb-[2px] bg-green-500 rounded-full"
                    class:bg-yellow-500=move || branch_state_read.get().eq(&BranchState::New)
                    class:bg-green-500=move || branch_state_read.get().eq(&BranchState::Online)
                    class:bg-red-500=move || branch_state_read.get().eq(&BranchState::Offline)
                    title={move || format!("{:?}", branch_state_read.get())}
                ></span>
            </h1>
        </div>
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
struct BranchDataSelectedDevices {
    selected_devices: Vec<Uuid>,
}

#[component]
pub fn BranchCharts(branch: BranchData, branch_devices: Vec<Device>) -> impl IntoView {
    let maybe_selected_devices = match branch.data.clone() {
        Some(data) => {
            let data_deserialized_result: Result<BranchDataSelectedDevices, _> =
                serde_json::from_value(data.clone());
            match data_deserialized_result {
                Ok(result) => Some(result.selected_devices),
                Err(_) => None,
            }
        }
        None => None,
    };
    let mut energy_overview_fields = vec![];
    let mut victron_loaded = false;
    let mut tasmota_count = 0;
    for device in branch_devices {
        if match &maybe_selected_devices {
            Some(selected_devices) => selected_devices.contains(&device.id),
            None => device.enabled,
        } {
            match &device.device_type_id {
                1 => {
                    // Victron
                    if victron_loaded {
                        continue;
                    }
                    energy_overview_fields.push(EnergyOverviewField {
                        device_id: device.id.clone(),
                        name: "AcConsumptionPower".to_string(),
                        enabled: true,
                    });
                    energy_overview_fields.push(EnergyOverviewField {
                        device_id: device.id.clone(),
                        name: "PvPower".to_string(),
                        enabled: true,
                    });
                    energy_overview_fields.push(EnergyOverviewField {
                        device_id: device.id.clone(),
                        name: "BatterySoc".to_string(),
                        enabled: true,
                    });
                    victron_loaded = true;
                }
                2 => {
                    // Tasmota
                    if tasmota_count >= 2 {
                        continue;
                    }
                    energy_overview_fields.push(EnergyOverviewField {
                        device_id: device.id.clone(),
                        name: "power".to_string(),
                        enabled: true,
                    });
                    tasmota_count += 1;
                }
                _ => {}
            }
        }
    }

    view! {
        <div class="container mx-auto border-t border-gray-200 dark:border-gray-700">
            <div class="max-w-[100vw] mt-5 mb-6 grid gap-4 xl:grid-cols-2">
                <div class="xl:order-2 max-w-[100vw] min-h-[388px] bg-white border rounded-md dark:bg-slate-800 dark:border-slate-600 shadow-xl text-sm text-gray-600 dark:text-gray-400">
                    <energy_overview_js::EnergyOverviewJS
                        branch_id=branch.id
                        name="energyOverview-yesterday".to_string()
                        initial_datetime=Local::now()
                        sample_unit=sumon_common::TimeSeriesSampleUnit::Hour
                        fields=energy_overview_fields.clone()
                    ></energy_overview_js::EnergyOverviewJS>
                </div>
                <div class="xl:order-1 max-w-[100vw] min-h-[388px] bg-white border rounded-md dark:bg-slate-800 dark:border-slate-600 shadow-xl text-sm text-gray-600 dark:text-gray-400">
                    <energy_overview_js::EnergyOverviewJS
                        branch_id=branch.id
                        name="energyOverview-today".to_string()
                        initial_datetime=Local::now() - Days::new(1)
                        sample_unit=sumon_common::TimeSeriesSampleUnit::Hour
                        fields=energy_overview_fields.clone()
                    ></energy_overview_js::EnergyOverviewJS>
                </div>
            </div>
            <div class="max-w-[100vw] mt-5 mb-6 grid gap-4">
                <div class="min-h-[388px] bg-white border rounded-md dark:bg-slate-800 dark:border-slate-600 shadow-xl text-sm text-gray-600 dark:text-gray-400">
                    <energy_overview_js::EnergyOverviewJS
                        branch_id=branch.id
                        name="energyOverview-month".to_string()
                        initial_datetime=Local::now()
                        sample_unit=sumon_common::TimeSeriesSampleUnit::Day
                        fields=energy_overview_fields.clone()
                    ></energy_overview_js::EnergyOverviewJS>
                </div>
            </div>
        </div>
    }
}

/*
<div class="max-w-[100vw] overflow-hidden mt-6 mb-6 grid gap-4 xl:grid-cols-2">
    <div class="xl:order-2 bg-white border rounded-md dark:bg-slate-800 dark:border-slate-600 shadow-xl p-2 pb-3 pl-0 md:p-4 md:pl-1 text-sm text-gray-600 dark:text-gray-400">
        <energy_overview::EnergyOverview
            branch_id=id()
            initial_datetime=Local::now()
            sample_unit=sumon_common::TimeSeriesSampleUnit::Hour
            wide=false
        ></energy_overview::EnergyOverview>
    </div>
    <div class="xl:order-1 bg-white border rounded-md dark:bg-slate-800 dark:border-slate-600 shadow-xl p-2 pb-3 pl-0 md:p-4 md:pl-1 text-sm text-gray-600 dark:text-gray-400">
        <energy_overview::EnergyOverview
            branch_id=id()
            initial_datetime=Local::now() - Days::new(1)
            sample_unit=sumon_common::TimeSeriesSampleUnit::Hour
            wide=false
        ></energy_overview::EnergyOverview>
    </div>
</div>
<div class="max-w-[100vw] overflow-hidden mt-6 mb-6 grid gap-4">
    <div class="bg-white border rounded-md dark:bg-slate-800 dark:border-slate-600 shadow-xl p-2 pb-3 pl-0 md:p-4 md:pl-1 text-sm text-gray-600 dark:text-gray-400">
        <energy_overview::EnergyOverview
            branch_id=id()
            initial_datetime=Local::now()
            sample_unit=sumon_common::TimeSeriesSampleUnit::Day
            wide=true
        ></energy_overview::EnergyOverview>
    </div>
</div>
*/