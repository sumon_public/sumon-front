use crate::api::RequestResult;
use crate::api_query::UserBranchQueryKey;
use crate::utils::table_class_provider::CustomTableTailwindClassesPreset;
use crate::branch::branch_device_form::DeviceEntryForm;
use leptos::*;
use leptos_query::*;
use leptos_struct_table::*;
use serde::{Deserialize, Serialize};
use serde_json::{json, Map, Value};
use sumon_common::{BranchData, BranchUpdateRequest, Device, AuthRole};
use uuid::Uuid;

use crate::{api, api_query, GlobalState};

use super::branch_device_form::DeviceFormState;

#[component]
pub fn BranchDevices(branch_signal: RwSignal<BranchData>, branch_devices: Vec<Device>) -> impl IntoView {
    let device_form_signal: RwSignal<DeviceFormState> = create_rw_signal(DeviceFormState::Hidden);
    provide_context(device_form_signal);
    let devices_signal = create_rw_signal(branch_devices);
    provide_context(devices_signal);

    // let add_device = move |device: Device| {
    //     devices_signal.update(|devices| {
    //         devices.push(device);
    //     });
    // };

    move || view! {
        <div class="container mx-auto max-w-screen-xl p-4 pb-5">
            <h2 class="text-3xl mt-5 mb-3 pb-2 text-gray-700 dark:text-gray-400 border-b border-gray-200 dark:border-gray-700">
                Devices
            </h2>
            <div class="min-h-[500px]">
                <BranchDeviceList branch_signal devices=devices_signal.get()/>
                // <DeviceEntryForm branch_id=branch_signal.get().id device_form_signal add_device=Box::new(add_device)/>
                <DeviceEntryForm branch_id=branch_signal.get().id device_form_signal/>
            </div>

        // <h4 class="mt-5">Legacy list</h4>
        // <DeviceList branch_signal=branch_signal/>
        </div>
    }
}

#[derive(TableRow, Clone, Debug, Hash, PartialEq, Eq)]
#[table(impl_vec_data_provider)]
#[table(classes_provider = "CustomTableTailwindClassesPreset")]
pub struct DeviceRow {
    #[table(skip)]
    pub id: String,
    #[table(title = "", class = "float-left xl:float-none pr-1 xl:pr-5", renderer = "DeviceEnabledCellRenderer")]
    pub enabled: bool,
    #[table(class = "float-left xl:float-none pl-2 xl:pl-5 font-bold")]
    pub name: String,
    #[table(class = "italic mr-14 xl:mr-0")]
    pub label: Option<String>,
    #[table(title = "Type", renderer = "DeviceTypeCellRenderer")]
    pub device_type_id: i32,
    pub mqtt_topic: Option<String>,
    #[table(skip)]
    pub config: Option<String>,
    #[table(title = "Selected", renderer = "SelectedCellRenderer")]
    pub selected: bool,
    #[table(title = "Actions", renderer = "DeviceActionsCellRenderer")]
    pub actions: String,  // Use a string to handle the "Edit" button rendering
}
impl From<Device> for DeviceRow {
    fn from(device: Device) -> Self {
        DeviceRow {
            id: device.id.to_string(),
            enabled: device.enabled,
            name: device.name,
            label: device.label,
            device_type_id: device.device_type_id,
            mqtt_topic: device.mqtt_topic,
            config: device.config,
            selected: false,
            actions: device.id.to_string(),
        }
    }
}

#[allow(unused_variables)]
#[component]
fn DeviceTypeCellRenderer<F>(
    class: String,
    #[prop(into)] value: MaybeSignal<i32>,
    on_change: F,
    index: usize,
) -> impl IntoView
where
    F: Fn(i32) + 'static,
{
    let device_type_name = match value.get() {
        1 => "Victron Cerbo GX",
        2 => "Tasmota plug",
        _ => "Unknown",
    };
    view! { <td class=format!("{} {}", class, "clear-both")>{device_type_name}</td> }
}

#[allow(unused_variables)]
#[component]
fn DeviceEnabledCellRenderer<F>(
    class: String,
    #[prop(into)] value: MaybeSignal<bool>,
    on_change: F,
    index: usize,
) -> impl IntoView
where
    F: Fn(bool) + 'static,
{
    let state_indicator = match value.get() {
        true => {
            view! { <span class="inline-block w-3 h-3 mx-0 bg-green-500 rounded-full" title="Enabled"></span> }
        }
        false => {
            view! { <span class="inline-block w-3 h-3 mx-0 bg-red-500 rounded-full" title="Disabled"></span> }
        }
    };
    view! { <td class=class>{state_indicator}</td> }
}

#[allow(unused_variables)]
#[component]
fn SelectedCellRenderer<F>(
    class: String,
    #[prop(into)] value: MaybeSignal<bool>,
    on_change: F,
    index: usize,
) -> impl IntoView
where
    F: Fn(bool) + 'static,
{
    let value_change_signal: RwSignal<Option<bool>> = create_rw_signal(None);
    create_effect(move |_| {
        if let Some(value) = value_change_signal.get() {
            on_change(value);
        }
    });

    let state = expect_context::<RwSignal<GlobalState>>();
    let user_read = create_read_slice(state, |state| state.user.clone());
    let branch_signal = expect_context::<RwSignal<BranchData>>();
    let is_superuser = move || match user_read.get() {
        Some(user) => user.role.eq(&AuthRole::Superuser),
        None => false,
    };
    let is_allowed = move || branch_signal.get().is_owner || is_superuser();

    move || view! {
        <td class=format!("{} {}", class, "clear-both")>
            <label class="inline-flex items-center cursor-pointer"
                class:opacity-60=!is_allowed()
                title="Select for charts">
                <SelectInputCheckbox value_read=value value_change_signal is_allowed=is_allowed/>
                <div class="relative w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
            </label>
        </td>
    }
}

#[component]
pub fn SelectInputCheckbox(
    value_read: MaybeSignal<bool>,
    value_change_signal: RwSignal<Option<bool>>,
    is_allowed: impl Fn() -> bool + 'static,
) -> impl IntoView {
    move || view! {
        <input
            type="checkbox"
            value="1"
            checked=if value_read.get() { Some(true) } else { None }
            disabled=if is_allowed() { None } else { Some(true) }
            class="sr-only peer"
            on:change=move |_evt| {
                value_change_signal
                    .set(
                        Some(
                            match value_change_signal.get() {
                                Some(old_value) => !old_value,
                                None => !value_read.get(),
                            },
                        ),
                    );
            }
        />
    }
}

#[allow(unused_variables)]
#[component]
fn DeviceActionsCellRenderer<F>(
    class: String,
    #[prop(into)] value: MaybeSignal<String>,
    on_change: F,
    index: usize,
) -> impl IntoView
where
    F: Fn(String) + 'static,
{
    let state = expect_context::<RwSignal<GlobalState>>();
    let user_read = create_read_slice(state, |state| state.user.clone());
    let branch_signal = expect_context::<RwSignal<BranchData>>();
    let is_superuser = move || match user_read.get() {
        Some(user) => user.role.eq(&AuthRole::Superuser),
        None => false,
    };
    let is_allowed = move || branch_signal.get().is_owner || is_superuser();

    let device_form_signal = expect_context::<RwSignal<DeviceFormState>>();
    let devices_signal = expect_context::<RwSignal<Vec<Device>>>();

    let change_edit_form = move || {
        match Uuid::parse_str(&value.get()) {
            Err(_) => { device_form_signal.set(DeviceFormState::Hidden); return; },
            Ok(selected_uuid) => {
                for device in devices_signal.get() {
                    if device.id.eq(&selected_uuid) {
                        device_form_signal.set(DeviceFormState::Some(device.clone()));
                        return;
                    }
                };
            }
        };
    };

    view! {
        <td class=format!("{} {}", class, "top-0 right-0 absolute xl:static pl-2 xl:pl-5")>
            <span
                class="inline-block align-middle fill-gray-500 hover:fill-black dark:fill-slate-300 dark:hover:fill-white text-white font-bold py-2 px-3 -my-1 rounded focus:outline-none focus:shadow-outline cursor-pointer"
                class:hidden=move || !is_allowed()
                title="Edit device"
                on:click=move |_| change_edit_form()
            >
                <svg id="icon_edit_1" data-name="Icon edit 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 122.88 119.19" class="h-5"><path class="cls-1" d="M104.84,1.62,121.25,18a5.58,5.58,0,0,1,0,7.88L112.17,35l-24.3-24.3L97,1.62a5.6,5.6,0,0,1,7.88,0ZM31.26,3.43h36.3L51.12,19.87H31.26A14.75,14.75,0,0,0,20.8,24.2l0,0a14.75,14.75,0,0,0-4.33,10.46v68.07H84.5A14.78,14.78,0,0,0,95,98.43l0,0a14.78,14.78,0,0,0,4.33-10.47V75.29l16.44-16.44V87.93A31.22,31.22,0,0,1,106.59,110l0,.05a31.2,31.2,0,0,1-22,9.15h-72a12.5,12.5,0,0,1-8.83-3.67l0,0A12.51,12.51,0,0,1,0,106.65v-72a31.15,31.15,0,0,1,9.18-22l.05-.05a31.17,31.17,0,0,1,22-9.16ZM72.33,74.8,52.6,80.9c-13.85,3-13.73,6.15-11.16-6.91l6.64-23.44h0l0,0L83.27,15.31l24.3,24.3L72.35,74.83l0,0ZM52.22,54.7l16,16-13,4c-10.15,3.13-10.1,5.22-7.34-4.55l4.34-15.4Z"/></svg>
            </span>
        </td>
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
struct BranchDataSelectedDevices {
    selected_devices: Vec<Uuid>,
}

#[component]
pub fn BranchDeviceList(
    branch_signal: RwSignal<BranchData>,
    devices: Vec<Device>,
) -> impl IntoView {
    let maybe_data = match branch_signal.get_untracked().data.clone() {
        Some(data) => {
            let data_deserialized_result: Result<BranchDataSelectedDevices, _> = serde_json::from_value(data.clone());
            match data_deserialized_result {
                Ok(result) => Some(result),
                Err(_) => None,
            }
        },
        None => None,
    };

    leptos::logging::log!("maybe_data: {:?}", maybe_data);
    let rows: Vec<DeviceRow> = devices
        .iter()
        .map(|device| {
            let mut device_row: DeviceRow = device.to_owned().into();
            if let Some(maybe_data) = maybe_data.clone() {
                device_row.selected = maybe_data.selected_devices.contains(&device.id);
            }
            device_row
        })
        .collect();

    let (rows_read, rows_write) = create_signal(rows);

    // // Compute the rows within reactive context
    // create_effect(move |_| {
    //     let maybe_data = branch_signal.with(|branch| match branch.data.clone() {
    //         Some(data) => {
    //             let data_deserialized_result: Result<BranchDataSelectedDevices, _> = serde_json::from_value(data.clone());
    //             match data_deserialized_result {
    //                 Ok(result) => Some(result),
    //                 Err(_) => None,
    //             }
    //         },
    //         None => None,
    //     });

    //     leptos::logging::log!("maybe_data: {:?}", maybe_data);
    //     let rows: Vec<DeviceRow> = devices
    //         .iter()
    //         .map(|device| {
    //             let mut device_row: DeviceRow = device.to_owned().into();
    //             if let Some(maybe_data) = maybe_data.clone() {
    //                 device_row.selected = maybe_data.selected_devices.contains(&device.id);
    //             }
    //             device_row
    //         })
    //         .collect();

    //     rows_write.set(rows);
    // });

    let on_change = move |evt: ChangeEvent<DeviceRow>| {
        // logging::log!(
        //     "Changed row at index {}:\n{:#?}",
        //     evt.row_index,
        //     evt.changed_row
        // );

        let mut selected_devices: Vec<String> = vec![];
        rows_write.update(|device_rows| {
            for device_row in device_rows {
                if device_row.id.eq(&evt.changed_row.id) {
                    *device_row = evt.changed_row.clone(); // replace changed row
                }
                if device_row.selected {
                    selected_devices.push(device_row.id.clone());
                }
            };
        });

        // get branch custom data
        let branch = branch_signal.get();
        let mut data_map = match branch.data.clone() {
            Some(Value::Object(map)) => map,
            _ => Map::new(),
        };

        data_map.insert("selected_devices".to_string(), selected_devices.into());
        let branch_update_request = BranchUpdateRequest {
            id: branch.id.clone(),
            name: branch.name.clone(),
            data: Some(json!(data_map)),
            position: branch.position.clone(),
        };

        spawn_local(async move {
            leptos::logging::log!("branch_update_request: {:?}", branch_update_request);
            let request_result = api::update_user_branch(branch_update_request).await;
            match api_query::handle_unauthorized::<BranchData>(request_result) {
                Ok(branch_data) => {
                    leptos::logging::log!("leptos log: api update_user_branch OK: {:?}", &branch_data);
                    let client = use_query_client();
                    // might invalidate whole query type, but since we already get the fresh data lets use it
                    // client.invalidate_query_type::<UserBranchQueryKey, RequestResult<BranchData>>();
                    client.set_query_data(UserBranchQueryKey(branch_data.id.clone().to_string()), RequestResult::Ok(branch_data.clone()));
                    // if invalidation causes problems, fallback to just updating branch_signal
                    // branch_signal.set(branch_data.clone());
                },
                Err(message) => {
                    leptos::logging::log!("update_user_branch failed: {}", message);
                },
            };
        });
    };

    move || view! {
        <div class="overflow-x-auto">
            <table class="w-full mt-3 mb-3">
                <TableContent rows=rows_read.get() on_change/>
            </table>
        </div>
    }
}
