mod api;
mod api_query;
mod components;
mod pages;
mod utils;
mod widgets;

use std::panic;

use components::menu_top::MenuTop;
use gloo_storage::*;
use leptos::*;
use leptos_meta::provide_meta_context;
use leptos_query::*;
use leptos_query_devtools::LeptosQueryDevtools;
use leptos_router::*;
use load_dotenv::load_dotenv;
use pages::{branch, home, login, logout};
use serde::{Deserialize, Serialize};
use sumon_common::UserData;
use uuid::Uuid;
use wasm_bindgen::prelude::*;

load_dotenv!(); // load environment variables from .env file

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[allow(dead_code)]
struct GlobalState {
    pub authenticated: bool,
    pub user_id: Option<Uuid>,
    pub dark_mode: Option<bool>,
    pub user: Option<UserData>,
}

impl GlobalState {
    // try load from LocalStorage
    fn load() -> Self {
        match gloo_storage::LocalStorage::get("global_state") {
            Ok(Some(state)) => state,
            _ => GlobalState::default(),
        }
    }

    // save to LocalStorage
    fn save(&self) {
        let _ = gloo_storage::LocalStorage::set("global_state", &self);
    }

    // clear LocalStorage and reset GlobalState
    #[allow(dead_code)]
    fn clear(&mut self) -> Self {
        gloo_storage::LocalStorage::delete("global_state");
        GlobalState::default()
    }
}

#[component]
fn App() -> impl IntoView {
    provide_meta_context();
    provide_query_client(); // Provides Query Client for entire app.

    let global_state = GlobalState::load();
    leptos::logging::log!("GlobalState::load(): {:#?}", global_state);
    provide_context(create_rw_signal(global_state));
    let state = expect_context::<RwSignal<GlobalState>>();
    // `create_slice` lets us create a "lens" into the data
    let (authenticated, _set_authenticated) = create_slice(
        // we take a slice *from* `state`
        state,
        // our getter returns a "slice" of the data
        |state| state.authenticated,
        // our setter describes how to mutate that slice, given a new value
        |state, n| state.authenticated = n,
    );

    view! {
        <LeptosQueryDevtools/>
        <Router>
            <MenuTop/>
            <main class="bg-[#F9FAFB] dark:bg-[#111827] h-full w-screen">
                // all our routes will appear inside <main>
                <Routes>
                    <ProtectedRoute
                        path="/"
                        view=login::Login
                        redirect_path="/home"
                        condition=move || !authenticated.get()
                    />
                    <ProtectedRoute
                        path="/logout"
                        view=logout::Logout
                        redirect_path="/"
                        condition=move || authenticated.get()
                    />
                    <ProtectedRoute
                        path="/home"
                        view=home::Home
                        redirect_path="/"
                        condition=move || authenticated.get()
                    />
                    <ProtectedRoute
                        path="/branch/:id"
                        view=branch::branch_detail::BranchDetail
                        redirect_path="/"
                        condition=move || authenticated.get()
                    />
                </Routes>
            </main>
        </Router>
    }
}

fn main() {
    // Set the log level
    // log::set_max_level(Level::Debug);
    log::set_max_level(log::LevelFilter::Debug);

    // display nicer panic errors in browser console
    panic::set_hook(Box::new(console_error_panic_hook::hook));

    // Initialize and mount your Leptos app
    leptos::mount_to_body(|| {
        view! { <App/> }
    })
}

// Import functions from js
#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);

    fn switchDarkMode(enable: bool);

    fn initJSChart(elementID: &str, options: &JsValue, dark_mode: bool);

    fn initDatePicker(elementID: &str, pickLevel: i32);
}
